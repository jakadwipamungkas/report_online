mainApp.controller('assessor', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', '$cookies', function($scope, httpHandler, appHelper, $filter, $timeout, $cookies){

  // Get Participant List
  $scope.getParticipant = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessor/api/process/participant',
		}).then(
			function successCallbacks(response) {

        $scope.dataParticipant = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.clickAdd = function () {
    $("#mdlAssessor").modal("show");
    $scope.getUsers();
    $scope.getParticipant();
  }

  $scope.pending = false;

  $scope.getData = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessor/api/process',
		}).then(
			function successCallbacks(response) {

        $scope.list_assessor = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getData();

  $scope.getUsers = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessor/api/process/users_data',
		}).then(
			function successCallbacks(response) {

        $scope.list_users = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.add = {
    participant_name: "",
    user_id: "",
    status: ""
  };

  $scope.add.password = "87654321";

  $scope.save = function () {
    console.log($scope.add);
    $('.btn-save').html('<i class="fas fa-spinner fa-spin"></i> Saving').attr('disabled');

    var formdata = new FormData();

    $.each($scope.add, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'assessor/api/process',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-save').html('<i class="fas fa-check-circle"></i> Save').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlAssessor").modal("hide");
          $scope.add = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlAssessor").modal("hide");
          $scope.add = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
    );
  }

  $scope.edit = {};
  $scope.clickEdit = function (value) {
    console.log(value);
    $scope.getUsers();
    $scope.getParticipant();
    $("#mdlEdit").modal("show");
    $scope.edit = value;
  }

  $scope.others = function () {
    if ($scope.add.user_id == "others") {
      $(".frm_others").removeClass('d-none');
    } else {
      $(".frm_others").addClass('d-none');
    }
  }

}]);
$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});

$(document).ready( function () {
  
  $('#tbl-assessor').DataTable();
  
});
  
// $("#sortable-table tbody").sortable({
//     handle: '.sort-handler'
// });