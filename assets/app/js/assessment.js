mainApp.controller('assessment', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', '$cookies', '$compile', function($scope, httpHandler, appHelper, $filter, $timeout, $cookies, $compile){

  $scope.assessreport = {
    participant_id: "",
    participant_position: "",
    area_kekuatan: "",
    competences: [],
    competences_pengembangan: [],
    glossary: [],
    behaviors: [],
    area_pengembangan: "",
    competence_name: "",
    mandiri: [""],
    penugasan: [""],
    training_seminar: [""]
  }

  // $scope.loadbehavior = false;
  $scope.loadglossary = false;

  // Get Participant List
  $scope.getParticipant = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/participant',
		}).then(
			function successCallbacks(response) {

        $scope.dataParticipant = response.data.data;
        // console.log($scope.dataParticipant);

			},
			function errorCallback(response) {

			}
		);

	}

  $scope.getParticipant();

  $scope.setParticipant = function () {
    // console.log($scope.assessreport.participant_id);
    httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/detailparticipant',
      params: {
        participant_id: $scope.assessreport.participant_id
      }
		}).then(
			function successCallbacks(response) {

        $scope.assessreport.participant_number = response.data.data.participant_number;
        $scope.assessreport.participant_name = response.data.data.participant_name;
        $scope.assessreport.participant_position = response.data.data.position;
        $scope.assessreport.area_kekuatan = "Sdr. " + response.data.data.participant_name;
        $scope.assessreport.area_pengembangan = "Pada dasarnya Sdr. " + response.data.data.participant_name;

			},
			function errorCallback(response) {

			}
		);
  }

  $scope.ringkasanindividu = {
    behaviors: "",
    competences: ""
  }
  
  $scope.saveRingkasan = function () {
    // console.log($scope.ringkasanindividu);
  }

  // Get Competence List
  $scope.getCompetence = function (behavior_id) {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/competence',
      params: {
        id_competence: behavior_id
      }
		}).then(
			function successCallbacks(response) {

        $scope.assessreport.competences_pengembangan = response.data.data.competence_name;

			},
			function errorCallback(response) {

			}
		);

	}

  $scope.getBehaviors = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/behavior',
		}).then(
			function successCallbacks(response) {

        $scope.list_behavior = response.data.data;
        $scope.list_behavior_area_pengembangan = response.data.data;

			},
			function errorCallback(response) {

			}
		);

	}

  $scope.getBehaviors();

  $scope.setBehaviors = function () {

    httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/glossary',
      params: {
        id_behavior: $scope.multiformassess[$scope.multiformassess.length-1].behaviors
      }
		}).then(
			function successCallbacks(response) {
        console.log(response.data.data.uraian_negatif);
        $scope.multiformassess[$scope.multiformassess.length-1].glossary = response.data.data;
        $scope.multiformassess[$scope.multiformassess.length-1].competences = response.data.competences_data;
        // console.log($scope.multiformassess[$scope.multiformassess.length-1].glossary.uraian_positif);

        $scope.loadglossary = true;
			},
			function errorCallback(response) {


			}
		);

  }

  $scope.multiformassess = [{
    behaviors: "",
    competences: "",
    glossarypositif: "",
    glossarynegatif: "",
    glossary: ""
  }];

  $scope.addRowkekuatan = function () {
    $scope.inserted = {
      behaviors: "",
      competences: "",
      glossarypositif: "",
      glossarynegatif: ""
    };

    $scope.multiformassess.push($scope.inserted);
  }

  $scope.removeColumn = function(column) {
    // console.log(column);
    // var index = $scope.multiformassess.indexOf(column);
    $scope.multiformassess.splice(column,1);

    // console.log($scope.multiformassess);
  };

  $scope.setBehaviorsPengembangan = function () {

    httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/glossary',
      params: {
        id_behavior: $scope.multiformpengembangan[$scope.multiformpengembangan.length-1].behaviors
      }
		}).then(
			function successCallbacks(response) {

        $scope.multiformpengembangan[$scope.multiformpengembangan.length-1].glossary = response.data.data;
        $scope.multiformpengembangan[$scope.multiformpengembangan.length-1].competences = response.data.competences_data;

        $scope.loadglossary = true;

			},
			function errorCallback(response) {

			}
		);

  }

  $scope.multiformpengembangan = [{
    behaviors: "",
    competences: "",
    glossarypositif: "",
    glossarynegatif: "",
    glossary: ""
  }];
  
  $scope.addRowPengembangan = function () {
    $scope.inserted = {
      behaviors: "",
      competences: "",
      glossarypositif: "",
      glossarynegatif: ""
    };

    $scope.multiformpengembangan.push($scope.inserted);
  }

  $scope.removeColumnPengembangan = function(column) {
    // console.log(column);
    // var index = $scope.multiformassess.indexOf(column);
    $scope.multiformpengembangan.splice(column,1);

    // console.log($scope.multiformpengembangan);
  };

  // Get Form Evaluasi
  $scope.dataevaluasi = function () {
      httpHandler.send({
        method: 'GET',
        url: mainUrl + 'assessment/api/process/comphior',
      }).then(
        function successCallbacks(response) {

          $scope.listevaluasi = response.data.data;
          // console.log($scope.listevaluasi);

        },
        function errorCallback(response) {

        }
      );
  }

  $scope.dataevaluasi();

  // Get Competence List
  $scope.getListCompetence = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/listcomp',
		}).then(
			function successCallbacks(response) {

        $scope.list_competence = response.data.data;
        // console.log($scope.list_competence);

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getListCompetence();

  $scope.setBhp = function () {
    $scope.getCompgm($scope.assessreport.competences_pengembangan);
  }

  $scope.getCompgm = function (competences_pengembangan) {
    httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/dtbehavior',
      params: {
        idcomp : competences_pengembangan
      }
		}).then(
			function successCallbacks(response) {

        $scope.list_behavior_pengembangan = response.data.data;

			},
			function errorCallback(response) {

			}
		);
  }

  // $scope.formassess.arrstatement = [];

  // $scope.setStatement = function () {
    console.log($scope.dtstatement);
  // }

  $scope.setPgm = function () {
    $scope.getPengembangan($scope.assessreport.competences_pengembangan, $scope.assessreport.behavior_name);    
  }

  $scope.getPengembangan = function (competence_id, behavior_id) {
    // console.log("tes");
    httpHandler.send({
			method: 'GET',
			url: mainUrl + 'assessment/api/process/pengembangan',
      params: {
        idcompetence: competence_id,
        idbehavior: behavior_id
      }
		}).then(
			function successCallbacks(response) {

        // console.log(response.data.data);
        $scope.list_mandiri           = response.data.data.mandiri;
        $scope.list_training_seminar  = response.data.data.training_seminar;
        $scope.list_penugasan         = response.data.data.penugasan;

			},
			function errorCallback(response) {

			}
		);
  }

  $scope.multiMandiri= function(mod,arr){
    if(mod=="add"){
        // $scope.pengembangan.mandiri.push(undefined);
        $scope.assessreport.mandiri.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.assessreport.mandiri.length>1)
        $scope.assessreport.mandiri.splice(arr, 1);
        else $scope.assessreport.mandiri[0]="";

    }

    // console.log("arr mandiri",$scope.assessreport);
  }

  $scope.multiPenugasan= function(mod,arr){
    if(mod=="add"){
        // $scope.pengembangan.penugasan.push(undefined);
        $scope.assessreport.penugasan.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.assessreport.penugasan.length>1)
        $scope.assessreport.penugasan.splice(arr, 1);
        else $scope.assessreport.penugasan[0]="";

    }

    // console.log("arr penugasan",$scope.assessreport);
  }

  $scope.multiTraining_seminar= function(mod,arr){
    if(mod=="add"){
        // $scope.pengembangan.training_seminar.push(undefined);
        $scope.assessreport.training_seminar.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.assessreport.training_seminar.length>1)
        $scope.assessreport.training_seminar.splice(arr, 1);
        else $scope.assessreport.training_seminar[0]="";

    }

    // console.log("arr training_seminar",$scope.assessreport);
  }
  

}]);

$(document).ready(function() {
    $('.participant_name').select2();
});