mainApp.controller('qualitycontrol', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', '$cookies', function($scope, httpHandler, appHelper, $filter, $timeout, $cookies){

  $scope.add = {
    qc_uid: "",
    assessor_id: "",
    status: "",
    participant_name: ""
  };

  $scope.add.password = "87654321";
  
  $scope.setData = function () {
    console.log($scope.add.assessor_id);
    if ($scope.add.assessor_id != "") {
      httpHandler.send({
        method: 'GET',
        url: mainUrl + 'qualitycontrol/api/process/detail_assess',
        params: {
          assessor_id: $scope.add.assessor_id
        }
      }).then(
        function successCallbacks(response) {
  
          $scope.dataassess = response.data.data;
          console.log($scope.dataassess);
          $scope.add.participant_name = $scope.dataassess;
  
        },
        function errorCallback(response) {
  
        }
      );
    }
  }

  $scope.clickAdd = function () {
    $("#mdlAdd").modal("show");
    $scope.getAssessor();
    $scope.getUsers();
  }

  $scope.getAssessor = function () {
    httpHandler.send({
			method: 'GET',
			url: mainUrl + 'qualitycontrol/api/process/assessor',
		}).then(
			function successCallbacks(response) {

        $scope.list_assessor = response.data.data;

			},
			function errorCallback(response) {

			}
		);
  }

  $scope.pending = false;

  $scope.getData = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'qualitycontrol/api/process',
		}).then(
			function successCallbacks(response) {

        $scope.list_qualitycontrol = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getData();

  $scope.getUsers = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'qualitycontrol/api/process/users_data',
		}).then(
			function successCallbacks(response) {

        $scope.list_users = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.save = function () {
    console.log($scope.add);
    $('.btn-save').html('<i class="fas fa-spinner fa-spin"></i> Saving').attr('disabled');

    var formdata = new FormData();

    $.each($scope.add, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'qualitycontrol/api/process',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-save').html('<i class="fas fa-check-circle"></i> Save').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlAdd").modal("hide");
          $scope.add = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 1000);

        } else {

          $("#mdlAdd").modal("hide");
          $scope.add = {};
          // appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
    );
  }

  $scope.editqc = {};
  $scope.clickEdit = function (value) {
    $scope.editqc = value;
    $("#mdlEdit").modal("show");
    $scope.getAssessor();
    $scope.getUsers();
  }

  $scope.others = function () {
    if ($scope.add.qc_uid == "others") {
      $(".frm_others").removeClass('d-none');
    } else {
      $(".frm_others").addClass('d-none');
    }
  }

}]);
$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});

// $(document).ready( function () {
  
//   $('#tbl-qualitycontrol').DataTable();
  
// });