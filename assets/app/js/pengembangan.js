mainApp.controller('pengembangan', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', '$cookies', function($scope, httpHandler, appHelper, $filter, $timeout, $cookies){

  // Get Competence List
  $scope.getCompetence = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'pengembangan/api/process/competence',
		}).then(
			function successCallbacks(response) {

        $scope.list_competence = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getCompetence();

  $scope.pengembangan = {
    competence_id: "",
    mandiri: [""],
    penugasan: [""],
    training_seminar: [""]
  };

  $scope.loaddata = false;

  // Get Behavior List
  $scope.getBehavior = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'pengembangan/api/process/behavior',
      params: {
        competence_id: $scope.pengembangan.competence_id
      }
		}).then(
			function successCallbacks(response) {
        
        $scope.loaddata = false;
        $scope.list_behavior = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.setBehavior = function () {
    $scope.loaddata = true;
    $scope.getBehavior();
  }

  $scope.pending = false;

  $scope.getData = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'pengembangan/api/process',
		}).then(
			function successCallbacks(response) {

        $scope.list_pengembangan = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getData();

  $scope.multiMandiri= function(mod,arr){
      if(mod=="add"){
          // $scope.pengembangan.mandiri.push(undefined);
          $scope.pengembangan.mandiri.splice(arr+1, 0,undefined);
      }
      else{
          if($scope.pengembangan.mandiri.length>1)
          $scope.pengembangan.mandiri.splice(arr, 1);
          else $scope.pengembangan.mandiri[0]="";

      }

      console.log("arr mandiri",$scope.pengembangan);
  }

  $scope.multiPenugasan= function(mod,arr){
    if(mod=="add"){
        // $scope.pengembangan.penugasan.push(undefined);
        $scope.pengembangan.penugasan.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.pengembangan.penugasan.length>1)
        $scope.pengembangan.penugasan.splice(arr, 1);
        else $scope.pengembangan.penugasan[0]="";

    }

    console.log("arr penugasan",$scope.pengembangan);
  }

  $scope.multiTraining= function(mod,arr){
    if(mod=="add"){
        // $scope.pengembangan.training_seminar.push(undefined);
        $scope.pengembangan.training_seminar.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.pengembangan.training_seminar.length>1)
        $scope.pengembangan.training_seminar.splice(arr, 1);
        else $scope.pengembangan.training_seminar[0]="";

    }

    console.log("arr training",$scope.pengembangan);
  }

  // Save Data

  $scope.save = function () {
    console.log($scope.pengembangan);
    $('.btn-save').html('<i class="fas fa-spinner fa-spin"></i> Saving').attr('disabled');

    // var formdata = new FormData();

    // $.each($scope.pengembangan, function (index, val) {
    //   formdata.append(index, val);
    // });

    // formdata.append("mandiri", JSON.stringify($scope.pengembangan.mandiri));
    // formdata.append("penugasan", JSON.stringify($scope.pengembangan.penugasan));
    // formdata.append("training", JSON.stringify($scope.pengembangan.training_seminar));

    // httpHandler.send({
		// 	method: 'POST',
		// 	url: mainUrl + 'pengembangan/api/process',
    //   data: formdata,
    //   headers: {
		// 		'Content-Type': undefined
		// 	}
		// }).then(
		// 	function successCallbacks(response) {

    //     $('.btn-save').html('<i class="fas fa-check-circle"></i> Save').removeAttr('disabled');

    //     if (response.data.error == false) {

    //       $scope.pengembangan = {};
    //       appHelper.showMessage('success', response.data.message);
    //       $timeout( function(){
    //           $('#popup-msg').modal('hide');
    //           window.location.href = mainUrl+'pengembangan';
	  //       }, 1000);

    //     } else {

    //       appHelper.showMessage('error', response.data.message);

    //     }

		// 	},
		// 	function errorCallback(response) {

		// 	}
		// );

  }

  // Edit Data
  $scope.editpengembangan = {};

  $scope.clickEdit = function(id) {
    // console.log($scope.list_pengembangan);
    $cookies.put('_clickEdit', JSON.stringify($scope.list_pengembangan[id].id));
    
    $timeout( function(){
        window.location.href = mainUrl+'pengembangan/form_edit';
    }, 100);
  }

  $scope.getPengembanganID = function() {
    var id = JSON.parse($cookies.get('_clickEdit'));
    $scope.getEditPengembangan(id);
  }

  $scope.getEditPengembangan = function (id) {
    httpHandler.send({
      method: "GET",
      url: mainUrl+'pengembangan/api/process/get_edit',
      params: {
        pengembangan_id: id
      }
  }).then(
      function successCallbacks(response){  
          $scope.pengembangan = response.data.data;
          $scope.pengembangan.competence_id = $scope.pengembangan.competence_id;
          $scope.getBehavior();
          $cookies.remove('_editNews');

      },
      function errorCallback(response) {
          // appHelper.showMessage('error', 'Ooppss something wrong, please try again!');
      });
  }

  
  if ($("#url").val() == "form_edit") {
    $scope.getPengembanganID();
  }
  
  $scope.update = function () {

    console.log($scope.pengembangan);
    $('.btn-update').html('<i class="fas fa-spinner fa-spin"></i> Updating').attr('disabled');

    var formdata = new FormData();

    $.each($scope.pengembangan, function (index, val) {
      formdata.append(index, val);
    });

    formdata.append("mandiri", JSON.stringify($scope.pengembangan.mandiri));
    formdata.append("penugasan", JSON.stringify($scope.pengembangan.penugasan));
    formdata.append("training", JSON.stringify($scope.pengembangan.training_seminar));

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'pengembangan/api/process/update',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-update').html('<i class="fas fa-check-circle"></i> Update').removeAttr('disabled');

        if (response.data.error == false) {

          $scope.pengembangan = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              window.location.href = mainUrl+'pengembangan';
	        }, 2000);

        } else {

          appHelper.showMessage('error', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

  // Delete Data
  $scope.deletepengembangan = {};
  $scope.clickDelete = function (value) {

    $("#mdlDelete").modal("show");
    $scope.deletepengembangan = value;

  }

  $scope.delete = function () {

    $('.btn-delete').html('<i class="fas fa-spinner fa-spin"></i> Deleting').attr('disabled');

    var formdata = new FormData();

    $.each($scope.deletepengembangan, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'pengembangan/api/process/delete',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-delete').html('<i class="fas fa-check-circle"></i> Deleting').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlDelete").modal("hide");
          $scope.pengembangan = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlDelete").modal("hide");
          $scope.pengembangan = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

}]);

$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});

$(document).ready( function () {
  
  $('#tbl-pengembangan').DataTable();
  
});