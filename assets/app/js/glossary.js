mainApp.controller('glossary', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', function($scope, httpHandler, appHelper, $filter, $timeout){

  $scope.glossary = {
    competence_id: "",
    behavior_id: "",
    uraian_positif: [""],
    uraian_negatif: [""]
  };

  // Get Competence List
  $scope.getCompetence = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'glossary/api/process/competence',
		}).then(
			function successCallbacks(response) {

        $scope.list_competence = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getCompetence();

  // Get Behavior List
  $scope.getBehavior = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'glossary/api/process/behavior',
      params: {
        competence_id: $scope.glossary.competence_id
      }
		}).then(
			function successCallbacks(response) {

        $scope.list_behavior = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.pending = false;

  // $scope.getData = function () {
	// 	$scope.pending = true;

	// 	httpHandler.send({
	// 		method: 'GET',
	// 		url: mainUrl + 'glossary/api/process',
	// 	}).then(
	// 		function successCallbacks(response) {

  //       $scope.list_glossary = response.data.data;

	// 		},
	// 		function errorCallback(response) {

	// 		}
	// 	);
	// }

  // $scope.getData();

  $scope.multiPositif= function(mod,arr){
      if(mod=="add"){
          $scope.glossary.uraian_positif.splice(arr+1, 0,undefined);
      }
      else{
          if($scope.glossary.uraian_positif.length>1)
          $scope.glossary.uraian_positif.splice(arr, 1);
          else $scope.glossary.uraian_positif[0]="";

      }

      console.log("arr uraian_positif",$scope.glossary);
  }

  $scope.multiNegatif= function(mod,arr){
    if(mod=="add"){
        $scope.glossary.uraian_negatif.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.glossary.uraian_negatif.length>1)
        $scope.glossary.uraian_negatif.splice(arr, 1);
        else $scope.glossary.uraian_negatif[0]="";

    }

    console.log("arr uraian_positif",$scope.glossary);
  }

  $scope.multiPositifEdit = function(mod,arr){
    if(mod=="add"){
        $scope.editglossary.uraian_positif.splice(arr+1, 0,undefined);
    }
    else{
        if($scope.editglossary.uraian_positif.length>1)
        $scope.editglossary.uraian_positif.splice(arr, 1);
        else $scope.editglossary.uraian_positif[0]="";

    }

    console.log("arr uraian_positif",$scope.editglossary);
}

$scope.multiNegatifEdit = function(mod,arr){
  if(mod=="add"){
      $scope.editglossary.uraian_negatif.splice(arr+1, 0,undefined);
  }
  else{
      if($scope.editglossary.uraian_negatif.length>1)
      $scope.editglossary.uraian_negatif.splice(arr, 1);
      else $scope.editglossary.uraian_negatif[0]="";

  }

  console.log("arr uraian_positif",$scope.editglossary);
}

  // Save Data

  $scope.clickAdd = function () {
    $("#mdlAdd").modal("show");
  }

  $scope.save = function () {

    $('.btn-save').html('<i class="fas fa-spinner fa-spin"></i> Saving').attr('disabled');

    // var formdata = new FormData();

    // $.each($scope.glossary, function (index, val) {
    //   formdata.append(index, val);
    // });

    // formdata.append("positif", JSON.stringify($scope.glossary.uraian_positif));
    // formdata.append("negatif", JSON.stringify($scope.glossary.uraian_negatif));

    // httpHandler.send({
		// 	method: 'POST',
		// 	url: mainUrl + 'glossary/api/process',
    //   data: formdata,
    //   headers: {
		// 		'Content-Type': undefined
		// 	}
		// }).then(
		// 	function successCallbacks(response) {

    //     $('.btn-save').html('<i class="fas fa-check-circle"></i> Save').removeAttr('disabled');

    //     if (response.data.error == false) {

    //       $("#mdlAdd").modal("hide");
    //       $scope.glossary = {
    //         competence_id: "",
    //         behavior_id: "",
    //         uraian_positif: [""],
    //         uraian_negatif: [""]
    //       };
    //       appHelper.showMessage('success', response.data.message);
    //       $timeout( function(){
    //           $('#popup-msg').modal('hide');
    //           $scope.getData();
	  //       }, 2000);

    //     } else {

    //       $("#mdlAdd").modal("hide");
    //       $scope.glossary = {
    //         competence_id: "",
    //         behavior_id: "",
    //         uraian_positif: [""],
    //         uraian_negatif: [""]
    //       };
    //       appHelper.showMessage('success', response.data.message);

    //     }

		// 	},
		// 	function errorCallback(response) {

		// 	}
		// );

  }

  // Edit Data
  $scope.editglossary = {};
  $scope.clickEdit = function (value) {
    $("#mdlEdit").modal("show");
    $scope.editglossary = JSON.parse(window.atob(value));
    console.log($scope.editglossary);
    // console.log($scope.editglossary);
    $scope.glossary.competence_id = JSON.parse(window.atob(value)).competence_id;
    $scope.getBehavior();
  }
  $scope.update = function () {

    $('.btn-update').html('<i class="fas fa-spinner fa-spin"></i> Updating').attr('disabled');

    var formdata = new FormData();

    $.each($scope.editglossary, function (index, val) {
      formdata.append(index, val);
    });

    formdata.append("positif", JSON.stringify($scope.editglossary.uraian_positif));
    formdata.append("negatif", JSON.stringify($scope.editglossary.uraian_negatif));

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'glossary/api/process/update',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-update').html('<i class="fas fa-check-circle"></i> Update').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlEdit").modal("hide");
          $scope.editglossary = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlEdit").modal("hide");
          $scope.editglossary = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

  // Delete Data
  $scope.deleteglossary = {};
  $scope.clickDelete = function (id, glossary_name) {

    $("#mdlDelete").modal("show");
    $scope.deleteglossary = {
      id: id,
      glossary_name: glossary_name
    }

  }

  $scope.delete = function () {

    $('.btn-delete').html('<i class="fas fa-spinner fa-spin"></i> Deleting').attr('disabled');

    var formdata = new FormData();

    $.each($scope.deleteglossary, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'glossary/api/process/delete',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-delete').html('<i class="fas fa-check-circle"></i> Deleting').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlDelete").modal("hide");
          $scope.glossary = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlDelete").modal("hide");
          $scope.glossary = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

  $scope.setBehavior = function () {
    $scope.getBehavior();
  }

}]);
$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});