mainApp.controller('dashboard', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', '$cookies', function($scope, httpHandler, appHelper, $filter, $timeout, $cookies){

  // Get Users List
  $scope.getUsers = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'dashboard/api/process',
		}).then(
			function successCallbacks(response) {

        $scope.datausers = response.data;
        console.log($scope.datausers);

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getUsers();

  // Get Participant List
  $scope.getParticipant = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'dashboard/api/process/participant',
		}).then(
			function successCallbacks(response) {

        $scope.dataParticipant = response.data.data;
        console.log(response);
        $scope.totalParticipant = response.data.total;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getParticipant();

  $scope.showOnline = function () {
    $("#mdlOnline").modal("show");
  }

}]);

$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});