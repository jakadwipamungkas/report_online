mainApp.controller('users_list', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', '$cookies', function($scope, httpHandler, appHelper, $filter, $timeout, $cookies){
  
  $scope.pending = false;

  $scope.getData = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'users/api/process',
		}).then(
			function successCallbacks(response) {

        $scope.list_users = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getData();
  // Save Data
  $scope.users = {};

  $scope.clickAdd = function () {
    $("#mdlAdd").modal("show");
  }

  $scope.save = function () {

    $('.btn-save').html('<i class="fas fa-spinner fa-spin"></i> Saving').attr('disabled');

    var formdata = new FormData();

    $.each($scope.users, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'users/api/process',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-save').html('<i class="fas fa-check-circle"></i> Save').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlAdd").modal("hide");
          $scope.users = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlAdd").modal("hide");
          $scope.users = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

  // Edit Data
  $scope.editusers = {};
  $scope.clickEdit = function (value) {
    $("#mdlEdit").modal("show");
    $scope.editusers = JSON.parse(window.atob(value));
    console.log($scope.editusers.temp_pass);
  }
  $scope.update = function () {

    $('.btn-update').html('<i class="fas fa-spinner fa-spin"></i> Updating').attr('disabled');

    var formdata = new FormData();

    $.each($scope.editusers, function (index, val) {
      formdata.append(index, val);
    });

    formdata.append("status_password", $scope.status_password);

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'users/api/process/update',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-update').html('<i class="fas fa-check-circle"></i> Update').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlEdit").modal("hide");
          $scope.editusers = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlEdit").modal("hide");
          $scope.users = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

  //Change Pass
  $(".pass_edit").attr("disabled", true);
  $scope.status_password = "nochange";
  $scope.changePass = function () {
    $(".btn-change-pass").addClass("d-none");
    $(".btn-cancel-pass").removeClass("d-none");
    $(".pass_edit").removeAttr("disabled");
    $scope.status_password = "change";
  }

  $scope.cancelPass = function () {
    $(".btn-change-pass").removeClass("d-none");
    $(".btn-cancel-pass").addClass("d-none");
    $(".pass_edit").attr("disabled", true);
    $scope.status_password = "nochange";
  }

  // Delete Data
  $scope.deleteusers = {};
  $scope.clickDelete = function (id, users_name) {

    $("#mdlDelete").modal("show");
    $scope.deleteusers = {
      id: id,
      users_name: users_name
    }

  }

  $scope.delete = function () {

    $('.btn-delete').html('<i class="fas fa-spinner fa-spin"></i> Deleting').attr('disabled');

    var formdata = new FormData();

    $.each($scope.deleteusers, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'users/api/process/delete',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-delete').html('<i class="fas fa-check-circle"></i> Deleting').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlDelete").modal("hide");
          $scope.users = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlDelete").modal("hide");
          $scope.users = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }


  // USERS LOG
  $scope.getLog = function () {
		$scope.pending = true;

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'users/api/process/logdata',
		}).then(
			function successCallbacks(response) {

        $scope.list_log = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  $scope.getLog();

}]);
$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});