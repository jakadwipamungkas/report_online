mainApp.controller('competence', ['$scope', 'httpHandler', 'appHelper', '$filter', '$timeout', function($scope, httpHandler, appHelper, $filter, $timeout){

  // // Get Cluster List
  $scope.getCluster = function () {

		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'competence/api/process/cluster',
		}).then(
			function successCallbacks(response) {

        $scope.list_cluster = response.data.data;

			},
			function errorCallback(response) {

			}
		);
	}

  // $scope.getCluster();

  // $scope.pending = false;

  // $scope.getData = function () {
	// 	$scope.pending = true;

	// 	httpHandler.send({
	// 		method: 'GET',
	// 		url: mainUrl + 'competence/api/process',
	// 	}).then(
	// 		function successCallbacks(response) {

  //       $scope.list_competence = response.data.data;

	// 		},
	// 		function errorCallback(response) {

	// 		}
	// 	);
	// }

  // $scope.getData();

  // Save Data
  $scope.competence = {};

  $scope.clickAdd = function () {
    $("#mdlAdd").modal("show");
  }

  $scope.save = function () {

    $('.btn-save').html('<i class="fas fa-spinner fa-spin"></i> Saving').attr('disabled');

    // var formdata = new FormData();

    // $.each($scope.competence, function (index, val) {
    //   formdata.append(index, val);
    // });

    // httpHandler.send({
		// 	method: 'POST',
		// 	url: mainUrl + 'competence/api/process',
    //   data: formdata,
    //   headers: {
		// 		'Content-Type': undefined
		// 	}
		// }).then(
		// 	function successCallbacks(response) {

    //     $('.btn-save').html('<i class="fas fa-check-circle"></i> Save').removeAttr('disabled');

    //     if (response.data.error == false) {

    //       $("#mdlAdd").modal("hide");
    //       $scope.competence = {};
    //       appHelper.showMessage('success', response.data.message);
    //       $timeout( function(){
    //           $('#popup-msg').modal('hide');
    //           $scope.getData();
	  //       }, 2000);

    //     } else {

    //       $("#mdlAdd").modal("hide");
    //       $scope.competence = {};
    //       appHelper.showMessage('success', response.data.message);

    //     }

		// 	},
		// 	function errorCallback(response) {

		// 	}
		// );

  }

  // Edit Data
  $scope.editcompetence = {};
  $scope.clickEdit = function (value) {
    $("#mdlEdit").modal("show");
    $scope.editcompetence = JSON.parse(atob(value));
    $scope.getCluster();
    console.log($scope.editcompetence);
  }
  $scope.update = function () {

    $('.btn-update').html('<i class="fas fa-spinner fa-spin"></i> Updating').attr('disabled');

    var formdata = new FormData();

    $.each($scope.editcompetence, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'competence/api/process/update',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-update').html('<i class="fas fa-check-circle"></i> Update').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlEdit").modal("hide");
          $scope.competence = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlEdit").modal("hide");
          $scope.competence = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

  // Delete Data
  $scope.deletecompetence = {};
  $scope.clickDelete = function (id, competence_name) {

    $("#mdlDelete").modal("show");
    $scope.deletecompetence = {
      id: id,
      competence_name: competence_name
    }

  }

  $scope.delete = function () {

    $('.btn-delete').html('<i class="fas fa-spinner fa-spin"></i> Deleting').attr('disabled');

    var formdata = new FormData();

    $.each($scope.deletecompetence, function (index, val) {
      formdata.append(index, val);
    });

    httpHandler.send({
			method: 'POST',
			url: mainUrl + 'competence/api/process/delete',
      data: formdata,
      headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {

        $('.btn-delete').html('<i class="fas fa-check-circle"></i> Deleting').removeAttr('disabled');

        if (response.data.error == false) {

          $("#mdlDelete").modal("hide");
          $scope.competence = {};
          appHelper.showMessage('success', response.data.message);
          $timeout( function(){
              $('#popup-msg').modal('hide');
              $scope.getData();
	        }, 2000);

        } else {

          $("#mdlDelete").modal("hide");
          $scope.competence = {};
          appHelper.showMessage('success', response.data.message);

        }

			},
			function errorCallback(response) {

			}
		);

  }

}]);
$("[data-checkboxes]").each(function() {
    var me = $(this),
      group = me.data('checkboxes'),
      role = me.data('checkbox-role');
  
    me.change(function() {
      var all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
        checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
        dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
        total = all.length,
        checked_length = checked.length;
  
      if(role == 'dad') {
        if(me.is(':checked')) {
          all.prop('checked', true);
        }else{
          all.prop('checked', false);
        }
      }else{
        if(checked_length >= total) {
          dad.prop('checked', true);
        }else{
          dad.prop('checked', false);
        }
      }
    });
});