<div class="main-content" ng-controller="glossary">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Data Glossary Statement</h4>
                  <div class="card-header-action">
                    <a href="" ng-click="clickAdd()" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Competence</th>
                                <th>Behavior Name</th>
                                <th>Uraian Positif</th>
                                <th>Uraian Negatif</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <!-- <tr ng-repeat="(key, val) in list_glossary">
                                <td ng-bind="(key+1)"></td>
                                <td ng-bind="val.competence_name"></td>
                                <td ng-bind="val.behavior_name"></td>
                                <td>
                                  <ul style="list-style-type: none;" ng-repeat="(kpositif, vpositif) in val.uraian_positif">
                                    <li ng-bind="vpositif"></li>
                                  </ui>
                                </td>
                                <td>
                                  <ul style="list-style-type: none;" ng-repeat="(knegatif, vnegatif) in val.uraian_negatif">
                                    <li ng-bind="vnegatif"></li>
                                  </ui>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit(val)" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.competence_name)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr> -->
                              <?php foreach ($list_glossary["data"] as $key => $value) { ?>
                                <tr>
                                  <td><?= ($key+1) ?></td>
                                  <td><?= $value["competence_name"] ?></td>
                                  <td><?= $value["behavior_name"] ?></td>
                                  <td>
                                    <?php foreach (($value["uraian_positif"]) as $kpositif => $vpos) { ?>
                                        <ul>
                                          <li><?= $vpos ?></li>
                                        </ul>
                                    <?php } ?>
                                  </td>
                                  <td>
                                    <?php foreach (($value["uraian_negatif"]) as $knegatif => $vneg) { ?>
                                        <ul>
                                          <li><?= $vneg ?></li>
                                        </ul>
                                    <?php } ?>
                                  </td>
                                  <td>
                                      <div class="btn-group">
                                          <a href="" ng-click="clickEdit('<?= base64_encode(json_encode($value)) ?>')" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i></a>
                                          <a href="" ng-click="clickDelete(val.id, val.competence_name)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                      </div>
                                  </td>
                                </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
    <!-- Modal Add -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAddLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAddLabel">Add Glossary Statements</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="<?= base_url("glossary/process") ?>" method="POST">
            <div class="modal-body">
                <div class="form-group">
                  <label for="competence_name">Competence Name <span class="text-danger">*</span></label>
                  <select class="form-control" name="competence_id" id="competence_name" ng-model="glossary.competence_id" ng-change="setBehavior()">
                    <option value="" selected>-- Choose Competence --</option>
                    <option ng-repeat="(kc, vc) in list_competence" ng-bind="vc.competence_name" value="{{vc.id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="behavior_name">Behavior Name <span class="text-danger">*</span></label>
                  <select class="form-control" name="behavior_id" id="behavior_name" ng-model="glossary.behavior_id">
                    <option value="" selected>-- Choose Behavior --</option>
                    <option ng-repeat="(kb, vb) in list_behavior" ng-bind="vb.behavior_name" value="{{vb.id}}"></option>
                  </select>
                </div>
                <div class="form-group" ng-repeat="(a, b) in glossary.uraian_positif track by $index">
                  <label for="uraian_positif">Uraian Positif <span class="text-danger">*</span></label>
                  <div class="row">
                      <div class="col-10 col-md-10 col-lg-10">
                        <textarea class="form-control" name="uraian_positif[]" id="uraian_positif" ng-model="glossary.uraian_positif[a]" style="height: 130px;"></textarea>
                      </div>
                      <div class="col-2 col-md-2 col-lg-2">
                          <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiPositif('add',a)"><i class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiPositif('rm',a)"><i class="fas fa-times"></i></button>
                      </div>
                    </div>
                </div>
                <div class="form-group" ng-repeat="(a, b) in glossary.uraian_negatif track by $index">
                  <label for="uraian_negatif">Uraian Negatif <span class="text-danger">*</span></label>
                  <div class="row">
                      <div class="col-10 col-md-10 col-lg-10">
                        <textarea class="form-control" name="uraian_negatif[]" id="uraian_negatif" ng-model="glossary.uraian_negatif[a]" style="height: 130px;"></textarea>
                      </div>
                      <div class="col-2 col-md-2 col-lg-2">
                          <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiNegatif('add',a)"><i class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiNegatif('rm',a)"><i class="fas fa-times"></i></button>
                      </div>
                    </div>
                </div>
          
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="submit" class="btn btn-primary btn-block m-0 btn-save" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="mdlEdit" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlEditLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlEditLabel">Edit Glossary Statement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                  <label for="competence_name">Competence Name <span class="text-danger">*</span></label>
                  <select class="form-control" name="competence_name" id="competence_name" ng-model="editglossary.competence_id">
                    <option value="" selected>-- Choose Competence --</option>
                    <option ng-repeat="(kc, vc) in list_competence" ng-bind="vc.competence_name" value="{{vc.id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="behavior_name">Behavior Name <span class="text-danger">*</span></label>
                  <select class="form-control" name="behavior_name" id="behavior_name" ng-model="editglossary.behavior_id">
                    <option value="" selected>-- Choose Behavior --</option>
                    <option ng-repeat="(kb, vb) in list_behavior" ng-bind="vb.behavior_name" value="{{vb.id}}"></option>
                  </select>
                </div>
                <div class="form-group" ng-repeat="(a, b) in editglossary.uraian_positif track by $index">
                  <label for="uraian_positif">Uraian Positif <span class="text-danger">*</span></label>
                  <div class="row">
                      <div class="col-10 col-md-10 col-lg-10">
                        <textarea class="form-control" id="uraian_positif" ng-model="editglossary.uraian_positif[a]" style="height: 130px;"></textarea>
                      </div>
                      <div class="col-2 col-md-2 col-lg-2">
                          <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiPositif('add',a)"><i class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiPositif('rm',a)"><i class="fas fa-times"></i></button>
                      </div>
                    </div>
                </div>
                <div class="form-group" ng-repeat="(a, b) in editglossary.uraian_negatif track by $index">
                  <label for="uraian_negatif">Uraian Negatif <span class="text-danger">*</span></label>
                  <div class="row">
                      <div class="col-10 col-md-10 col-lg-10">
                        <textarea class="form-control" id="uraian_negatif" ng-model="editglossary.uraian_negatif[a]" style="height: 130px;"></textarea>
                      </div>
                      <div class="col-2 col-md-2 col-lg-2">
                          <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiNegatifEdit('add',a)"><i class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiNegatifEdit('rm',a)"><i class="fas fa-times"></i></button>
                      </div>
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0 btn-update" ng-click="update()"><i class="fas fa-check-circle"></i> Update</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" tabindex="-1" role="dialog" aria-labelledby="mdlDelete" aria-hidden="true">
      <div class="modal-dialog modal-sm modal-dialog-centered">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">
              <i class="fas fa-exclamation-circle text-warning mt-3 mb-5" style="font-size: 40px"></i><br>
              <h6>Are you sure want to delete this glossary statements <b class="text-danger" ng-bind="deleteglossary.glossary_name"></b> ?</h6>
              <form>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="glossary_delete_id" ng-model="deleteglossary.id">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="button" class="btn btn-primary btn-block m-0 btn-delete" ng-click="delete()"><i class="fas fa-check-circle"></i> Delete</button>
              </div>
            </div>
          </div>
      </div>
    </div> 
</div>