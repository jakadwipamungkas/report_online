<?php
class Glossary_model extends CI_Model {

    public function get()
    {
        $sql = $this->db->select("glossary.*, competence.competence_name, behavior.behavior_name")
                        ->from("glossary")
                        ->join("competence", "competence.id = glossary.competence_id")
                        ->join("behavior", "behavior.id = glossary.behavior_id")
                        ->get()
                        ->result_array();

        foreach ($sql as $key => $value) {
            $sql[$key]["uraian_positif"] = json_decode($value["uraian_positif"]);
            $sql[$key]["uraian_negatif"] = json_decode($value["uraian_negatif"]);
        }
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function getCompetence()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function getBehavior()
    {
        $sql = $this->db->select("id, behavior_name")
                        ->from("behavior")
                        ->where("competence_id", $this->get("competence_id"))
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function save($data)
    {

        $query = $this->db->insert("glossary", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully created, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function update($id, $data)
    {

        $query = $this->db->where("id", $id)->update("glossary", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully updated, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function delete($id)
    {

        $query = $this->db->where("id", $id)->delete("glossary");

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully deleted, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

}