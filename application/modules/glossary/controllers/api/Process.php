<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Glossary_model"
        ]);
	}

    public function competence_get()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function behavior_get()
    {
        $sql = $this->db->select("id, behavior_name")
                        ->from("behavior")
                        ->where("competence_id", $this->get("competence_id"))
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();

        // print_r("<pre>");

        $data = [
            "competence_id"    => $formData["competence_id"],
            "behavior_id"      => $formData["behavior_id"],
            "uraian_positif"   => $formData["positif"],
            "uraian_negatif"   => $formData["negatif"],
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        // print_r($formData);
        // exit();

        $sql = $this->Glossary_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "competence_id"    => $formData["competence_id"],
            "behavior_id"      => $formData["behavior_id"],
            "uraian_positif"   => $formData["positif"],
            "uraian_negatif"   => $formData["negatif"],
            "updated_by"       => $this->session->userdata("users")["fullname"],
            "updated_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Glossary_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Glossary_model->delete($formData["id"]);

        $this->response($sql);
    }
}
