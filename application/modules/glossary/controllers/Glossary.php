<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Glossary extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Glossary_model"
        ]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Glossary';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/glossary.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/glossary.css'
		);

		$this->data["list_glossary"] 	= $this->Glossary_model->get();
		$this->data["list_competence"] 	= $this->Glossary_model->getCompetence();

		insertLog("Glossary Statements", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

	public function process_post()
	{
		$formData = $this->post();
		// print_r("<pre>");
		// print_r(json_encode($formData["uraian_positif"]));
		// exit();
        $data = [
            "competence_id"    => $formData["competence_id"],
            "behavior_id"      => $formData["behavior_id"],
            "uraian_positif"   => json_encode($formData["uraian_positif"]),
            "uraian_negatif"   => json_encode($formData["uraian_negatif"]),
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Glossary_model->save($data);

        redirect("glossary");
	}
}
