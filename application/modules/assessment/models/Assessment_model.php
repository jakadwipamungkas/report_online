<?php
class Assessment_model extends CI_Model {

    public function getdatareport($request)
    {
        print_r("<pre>");

        $dt_p   =   $this->db->select("participant.*")
                            ->from("participant as participant")
                            ->where("participant_name", $request["participant_name"])
                            ->get()
                            ->first_row();
                            
        $dt_a   = $this->db->select("assessor.*, users.fullname")
                            ->from("assessor as assessor")
                            ->join("users", "users.id = assessor.user_id", "left")
                            ->where("participant_id", $dt_p->id)
                            ->get()
                            ->first_row();
        
        $dt_qc  = $this->db->select("quality_control.qc_uid, qc_degree, qc_sipp, users.fullname")
                            ->from("quality_control")
                            ->join("users", "users.id = quality_control.qc_uid", "left")
                            ->where("quality_control.qc_uid", $dt_p->qc_id)
                            ->where("quality_control.participant_id", $dt_p->id)
                            ->get()
                            ->first_row();

        $data = [
            "participant"   => $dt_p,
            "assessor"      => $dt_a,
            "qc"            => $dt_qc
        ];

        return $data;
    }

}