<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007;
use PhpOffice\PhpWord\Element\Field;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\SimpleType\TblWidth;
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Assessment_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("assessor.*, users.fullname, participant.name_participant")
                        ->from("assessor")
                        ->join("users", "users.id = assessor.user_id")
                        ->join("participant", "participant.id = assessor.participant_id")
                        ->where("assessor_id", $this->session->userdata("users")["id"])
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function participant_get()
	{
        $sql = $this->db->select("*")
                        ->from("participant")
                        ->where("assessor_id", $this->session->userdata("users")["id"])
                        ->where("status_report", 0)
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function detailparticipant_get()
	{
        $sql = $this->db->select("*")
                        ->from("participant")
                        ->where("id", $this->get("participant_id"))
                        ->get()
                        ->first_row();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function competence_get()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->where("id", $this->get("id_competence"))
                        ->get()
                        ->first_row();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    function behavior_get()
    {
        $sql = $this->db->select("*")
                        ->from("behavior")
                        // ->where("competence_id", $this->get("id_competence"))
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    function glossary_get()
    {
        $getbvior = $this->db->select("id")
                            ->from("behavior")
                            ->where("behavior_name", $this->get("id_behavior"))
                            ->get()
                            ->first_row();

        $sql = $this->db->select("*")
                        ->from("glossary")
                        ->where("behavior_id", $getbvior->id)
                        ->get()
                        ->first_row();
        
        // foreach ($sql as $key => $value) {
            $sql->uraian_positif = json_decode($sql->uraian_positif);
            $sql->uraian_negatif = json_decode($sql->uraian_negatif);
        // }

        $getcompetences = $this->db->select("competence.competence_name")
                                    ->from("behavior")
                                    ->join("competence", "competence.id = behavior.competence_id")
                                    ->where("behavior.id", $getbvior->id)
                                    ->get()
                                    ->first_row();

        // print_r("<pre>");
        // print_r($sql);
        // exit();
        $return = [
            "data" => $sql,
            "competences_data" => $getcompetences->competence_name
        ];

        $this->response($return);
    }

    public function comphior_get()
    {
        $arr = array();

        $sql  = $this->db->select("*")
                         ->from("competence")
                         ->get()
                         ->result_array();

        foreach ($sql as $key => $value) {
            $vior = $this->db->select("behavior_name")
                             ->from("behavior")
                             ->where("competence_id", $value["id"])
                             ->get()
                             ->result_array();

            $arr[$value["competence_name"]]["desc"] = $value["competence_desc"];
            $arr[$value["competence_name"]]["comp_nonspace"] = str_replace(" ", "_", $value["competence_name"]);
            $arr[$value["competence_name"]]["behavior"] = $vior;
        }

        $return = [
            "data" => $arr
        ];

        $this->response($return);
    }

    public function listcomp_get()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function dtbehavior_get()
    {
        $getcomp = $this->db->select("id")
                        ->from("competence")
                        ->where("competence_name", $this->get("idcomp"))
                        ->get()
                        ->first_row();

        $sql = $this->db->select("id, behavior_name")
                        ->from("behavior")
                        ->where("competence_id", $getcomp->id)
                        ->get()
                        ->result_array();

        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function pengembangan_get()
    {
        $getcomp = $this->db->select("id")
                        ->from("competence")
                        ->where("competence_name", $this->get("idcompetence"))
                        ->get()
                        ->first_row();

        $getbvior = $this->db->select("id")
                            ->from("behavior")
                            ->where("behavior_name", $this->get("idbehavior"))
                            ->get()
                            ->first_row();

        $sql = $this->db->select("mandiri, penugasan, training_seminar")
                        ->from("pengembangan")
                        ->where("competence_id", $getcomp->id)
                        ->where("behavior_id", $getbvior->id)
                        ->get()
                        ->first_row();

        if (!empty($sql)) {
            $sql->mandiri = json_decode($sql->mandiri);
            $sql->penugasan = json_decode($sql->penugasan);
            $sql->training_seminar = json_decode($sql->training_seminar);
        } else {
            $sql = [];
        }
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();

        $data = [
            "user_id"           => $formData["user_id"],
            "status"            => $formData["status"],
            "participant_id"    => $formData["participant_name"],
            "assessor_degree"   => $formData["degree"],
            "assessor_sipp"     => $formData["sipp"],
            "status_report"     => "waiting",
            "created_by"        => $this->session->userdata("users")["fullname"],
            "created_at"        => date("Y-m-d H:i:s")
        ];

        $sql = $this->assessment_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "user_id"           => $formData["user_id"],
            "status"            => $formData["status"],
            "participant_name"  => $formData["participant_name"],
            "assessor_degree"   => $formData["degree"],
            "assessor_sipp"     => $formData["sipp"],
            "status_report"     => "waiting",
            "updated_by"    => $this->session->userdata("users")["fullname"],
            "updated_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->assessment_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->assessment_model->delete($formData["id"]);

        $this->response($sql);
    }

    // SAVE TO DB BEFORE GENERATE TO WORD
    public function saving_post()
    {

        // START AREA PENGEMBANGAN
        $bvior_pengembang   = $this->post("behaviors_pengembang");
        $comp_pengembang    = $this->post("competences_pengembang");

        $arr_pengembang = [];

        $str_index = [];

        foreach ($bvior_pengembang as $key => $value) {
            $bvior_pengembang[$key] = str_replace(" ", "_", $value);
        }

        foreach ($bvior_pengembang as $key => $value) {
            if (array_key_exists('areapengembangan-'.$value, $this->post()) == 1) {
                foreach ($this->post('areapengembangan-'.$value) as $kcm => $vcm) {
                    $dt_cmp = $this->db->select("competence.competence_name")
                                        ->from("behavior")
                                        ->join("competence", "competence.id = behavior.competence_id")
                                        ->where("behavior_name", str_replace("_", " ", $value))
                                        ->get()
                                        ->first_row();

                    $data_pengembang = [
                        "competences"       => $dt_cmp->competence_name,
                        "list_statement"    => str_replace("areapengembangan-", "", $vcm)
                    ];
                    array_push($arr_pengembang, $data_pengembang);
                }    
            }
        }

        $arr_new_pengembang = array();

        foreach ($arr_pengembang as $key => $item) {
            $arr_new_pengembang[$item['competences']]["competences"][] = $item["competences"];
            $arr_new_pengembang[$item['competences']]["list_statement"][] = $item["list_statement"];
        }

        foreach ($arr_new_pengembang as $knew => $vnew) {
            foreach ($vnew["list_statement"] as $knew2 => $vnew2) {
                $final_array_pengembang[$knew] = array_unique(call_user_func_array('array_merge', $vnew["list_statement"]));
                $final_array_pengembang[$knew]["competences"] = $vnew["competences"][0];
            }
            $final_array_pengembang = array_values($final_array_pengembang);
        }

        foreach ($final_array_pengembang as $kfinal => $vfinal) {
            $arr_pengembang_new[$vfinal["competences"]] = $vfinal;
        }

        foreach ($arr_pengembang_new as $kfix => $vfix) {
            unset($vfix["competences"]);
            $final_area_pengembang[$kfix]["list_statement"] = $vfix;
        }

        // AREA PENGEMBANG V.2 TEMPLATE
        $arrlist_pengembang = [];
        foreach ($final_area_pengembang as $kdp => $vdp) {
            array_push($arrlist_pengembang, $vdp["list_statement"]);
        }

        $descarray_pengembang;
        foreach ($arrlist_pengembang as $kmrg => $vmrg) {
            $descarray_pengembang[] = implode(", ", str_replace(".", "", $vmrg));
        }
        // END AREA PENGEMBANGAN

        // START AREA KEKUATAN
        $bvior_kekuatan = $this->post("behaviors_kekuatan");
        $comp_kekuatan = $this->post("competences_kekuatan");

        $arr_kekuatan = [];

        $str_index = [];

        foreach ($bvior_kekuatan as $key => $value) {
            $bvior_kekuatan[$key] = str_replace(" ", "_", $value);
        }

        foreach ($bvior_kekuatan as $key => $value) {
            if (array_key_exists($value, $this->post()) == 1) {
                foreach ($this->post($value) as $kcm => $vcm) {
                    $dt_cmp = $this->db->select("competence.competence_name")
                                        ->from("behavior")
                                        ->join("competence", "competence.id = behavior.competence_id")
                                        ->where("behavior_name", str_replace("_", " ", $value))
                                        ->get()
                                        ->first_row();

                    $data_kekuatan = [
                        "competences"       => $dt_cmp->competence_name,
                        "list_statement"    => $vcm
                    ];
                    array_push($arr_kekuatan, $data_kekuatan);
                }    
            }
        }

        $arr_new_kekuatan = array();

        foreach ($arr_kekuatan as $key => $item) {
            $arr_new_kekuatan[$item['competences']]["competences"][] = $item["competences"];
            $arr_new_kekuatan[$item['competences']]["list_statement"][] = $item["list_statement"];
        }


        $arrlist = [];
        foreach ($arr_new_kekuatan as $knew => $vnew) {
            foreach ($vnew["list_statement"] as $knew2 => $vnew2) {
                $final_array[$knew] = array_unique(call_user_func_array('array_merge', $vnew["list_statement"]));
                $final_array[$knew]["competences"] = $vnew["competences"][0];
            }
            $final_array = array_values($final_array);
        }

        foreach ($final_array as $kfinal => $vfinal) {
            $arr_kekuatan_new[$vfinal["competences"]] = $vfinal;
        }
        
        foreach ($arr_kekuatan_new as $kfix => $vfix) {
            unset($vfix["competences"]);
            $final_area_kekuatan[$kfix]["list_statement"] = $vfix;
        }

        // AREA Kekuatan V.2 TEMPLATE
        $arrlist_kekuatan = [];
        foreach ($final_area_kekuatan as $kdk => $vdk) {
            array_push($arrlist_kekuatan, $vdk["list_statement"]);
        }

        $descarray_kekuatan;
        foreach ($arrlist_kekuatan as $kmrgk => $vmrgk) {
            $descarray_kekuatan[] = implode(", ", str_replace(".", "", $vmrgk));
        }
        // END AREA KEKUATAN

        // Cover letter, Pengesahan, Biodata Participant, pengantar
        $cnt = 0;
        $nilai = [];
        $arr_nilai = [];
        
        $fevaluasi = $this->post("formevaluasi");

        foreach ($fevaluasi as $kform => $vform) {
            foreach ($vform as $keval => $veval) {
                $nilai[$kform]["combination_score"][] = $keval;
                $cnt++;
            }
        }

        foreach ($nilai as $knilai => $vnilai) {
            foreach ($vnilai as $kn => $vn) {
                $str_arr = str_replace("_", "", $vn);
            }
            $arr_nilai[$knilai] = $str_arr;
        }

        $di = 0;
        foreach ($arr_nilai as $knilai2 => $vnilai2) {
            $arr_nilai[$knilai2] = preg_replace("/[^a-zA-Z]/", "", $vnilai2);
            $di++;
        }
        
        $arr_nilai2 = [];
        foreach ($arr_nilai as $knilai3 => $vnilai3) {
            sort($vnilai3);
            $arr_nilai[$knilai3] = $vnilai3;
        }
    
        foreach ($arr_nilai as $ktrim => $vtrim) {
            foreach ($vtrim as $key => $vtrim2) {
                $arr_nilai[$ktrim][$key] = substr($vtrim2, 1);
            }
        }
        
        foreach ($arr_nilai as $kf => $vf) {
            $arr_nilai[$kf] = implode(",",$vf);
        }

        $scoring = [];

        foreach ($arr_nilai as $key => $value) {
            $explo = explode(",",$value);

            $skorget = $this->db->select("nilai")
                            ->from("skoring")
                            ->where("competence", $key)
                            ->like("combination", implode(",", $explo))
                            ->get()
                            ->first_row();
            
            $scoring[$key] = !empty($skorget) ? $skorget->nilai : "";
        }

        $personal_quality = [
            'a1'                    => $scoring["Authenticity"] == 1 ? $scoring["Authenticity"] : "",
            'a2'                    => $scoring["Authenticity"] == 2 ? $scoring["Authenticity"] : "",
            'a3'                    => $scoring["Authenticity"] == 3 ? $scoring["Authenticity"] : "",
            'a4'                    => $scoring["Authenticity"] == 4 ? $scoring["Authenticity"] : "",
            'a5'                    => $scoring["Authenticity"] == 5 ? $scoring["Authenticity"] : "",

            'ag1'                    => $scoring["Agility"] == 1 ? $scoring["Agility"] : "",
            'ag2'                    => $scoring["Agility"] == 2 ? $scoring["Agility"] : "",
            'ag3'                    => $scoring["Agility"] == 3 ? $scoring["Agility"] : "",
            'ag4'                    => $scoring["Agility"] == 4 ? $scoring["Agility"] : "",
            'ag5'                    => $scoring["Agility"] == 5 ? $scoring["Agility"] : "",

            'ao1'                    => $scoring["Achievement_Orientation"] == 1 ? $scoring["Achievement_Orientation"] : "",
            'ao2'                    => $scoring["Achievement_Orientation"] == 2 ? $scoring["Achievement_Orientation"] : "",
            'ao3'                    => $scoring["Achievement_Orientation"] == 3 ? $scoring["Achievement_Orientation"] : "",
            'ao4'                    => $scoring["Achievement_Orientation"] == 4 ? $scoring["Achievement_Orientation"] : "",
            'ao5'                    => $scoring["Achievement_Orientation"] == 5 ? $scoring["Achievement_Orientation"] : "",

            'cl1'                    => $scoring["Continuous_Learning"] == 1 ? $scoring["Continuous_Learning"] : "",
            'cl2'                    => $scoring["Continuous_Learning"] == 2 ? $scoring["Continuous_Learning"] : "",
            'cl3'                    => $scoring["Continuous_Learning"] == 3 ? $scoring["Continuous_Learning"] : "",
            'cl4'                    => $scoring["Continuous_Learning"] == 4 ? $scoring["Continuous_Learning"] : "",
            'cl5'                    => $scoring["Continuous_Learning"] == 5 ? $scoring["Continuous_Learning"] : "",

            'fc1'                    => $scoring["Fostering_Collaboration"] == 1 ? $scoring["Fostering_Collaboration"] : "",
            'fc2'                    => $scoring["Fostering_Collaboration"] == 2 ? $scoring["Fostering_Collaboration"] : "",
            'fc3'                    => $scoring["Fostering_Collaboration"] == 3 ? $scoring["Fostering_Collaboration"] : "",
            'fc4'                    => $scoring["Fostering_Collaboration"] == 4 ? $scoring["Fostering_Collaboration"] : "",
            'fc5'                    => $scoring["Fostering_Collaboration"] == 5 ? $scoring["Fostering_Collaboration"] : "",

            'dp1'                    => $scoring["Developing_Partnerships"] == 1 ? $scoring["Developing_Partnerships"] : "",
            'dp2'                    => $scoring["Developing_Partnerships"] == 2 ? $scoring["Developing_Partnerships"] : "",
            'dp3'                    => $scoring["Developing_Partnerships"] == 3 ? $scoring["Developing_Partnerships"] : "",
            'dp4'                    => $scoring["Developing_Partnerships"] == 4 ? $scoring["Developing_Partnerships"] : "",
            'dp5'                    => $scoring["Developing_Partnerships"] == 5 ? $scoring["Developing_Partnerships"] : "",

            'dt1'                    => $scoring["Developing_Talent"] == 1 ? $scoring["Developing_Talent"] : "",
            'dt2'                    => $scoring["Developing_Talent"] == 2 ? $scoring["Developing_Talent"] : "",
            'dt3'                    => $scoring["Developing_Talent"] == 3 ? $scoring["Developing_Talent"] : "",
            'dt4'                    => $scoring["Developing_Talent"] == 4 ? $scoring["Developing_Talent"] : "",
            'dt5'                    => $scoring["Developing_Talent"] == 5 ? $scoring["Developing_Talent"] : "",

            'ch1'                    => $scoring["Facilitating_Change"] == 1 ? $scoring["Facilitating_Change"] : "",
            'ch2'                    => $scoring["Facilitating_Change"] == 2 ? $scoring["Facilitating_Change"] : "",
            'ch3'                    => $scoring["Facilitating_Change"] == 3 ? $scoring["Facilitating_Change"] : "",
            'ch4'                    => $scoring["Facilitating_Change"] == 4 ? $scoring["Facilitating_Change"] : "",
            'ch5'                    => $scoring["Facilitating_Change"] == 5 ? $scoring["Facilitating_Change"] : "",

            'lt1'                    => $scoring["Leading_Teams"] == 1 ? $scoring["Leading_Teams"] : "",
            'lt2'                    => $scoring["Leading_Teams"] == 2 ? $scoring["Leading_Teams"] : "",
            'lt3'                    => $scoring["Leading_Teams"] == 3 ? $scoring["Leading_Teams"] : "",
            'lt4'                    => $scoring["Leading_Teams"] == 4 ? $scoring["Leading_Teams"] : "",
            'lt5'                    => $scoring["Leading_Teams"] == 5 ? $scoring["Leading_Teams"] : "",

            'ca1'                    => $scoring["Commercial_Acumen"] == 1 ? $scoring["Commercial_Acumen"] : "",
            'ca2'                    => $scoring["Commercial_Acumen"] == 2 ? $scoring["Commercial_Acumen"] : "",
            'ca3'                    => $scoring["Commercial_Acumen"] == 3 ? $scoring["Commercial_Acumen"] : "",
            'ca4'                    => $scoring["Commercial_Acumen"] == 4 ? $scoring["Commercial_Acumen"] : "",
            'ca5'                    => $scoring["Commercial_Acumen"] == 5 ? $scoring["Commercial_Acumen"] : "",

            'od1'                    => $scoring["Operational_Decision_Making"] == 1 ? $scoring["Operational_Decision_Making"] : "",
            'od2'                    => $scoring["Operational_Decision_Making"] == 2 ? $scoring["Operational_Decision_Making"] : "",
            'od3'                    => $scoring["Operational_Decision_Making"] == 3 ? $scoring["Operational_Decision_Making"] : "",
            'od4'                    => $scoring["Operational_Decision_Making"] == 4 ? $scoring["Operational_Decision_Making"] : "",
            'od5'                    => $scoring["Operational_Decision_Making"] == 5 ? $scoring["Operational_Decision_Making"] : "",

            'po1'                    => $scoring[0] == 1 ? $scoring[0] : "",
            'po2'                    => $scoring[0] == 2 ? $scoring[0] : "",
            'po3'                    => $scoring[0] == 3 ? $scoring[0] : "",
            'po4'                    => $scoring[0] == 4 ? $scoring[0] : "",
            'po5'                    => $scoring[0] == 5 ? $scoring[0] : "",

            'cr1'                    => $scoring["Customer_Relationships"] == 1 ? $scoring["Customer_Relationships"] : "",
            'cr2'                    => $scoring["Customer_Relationships"] == 2 ? $scoring["Customer_Relationships"] : "",
            'cr3'                    => $scoring["Customer_Relationships"] == 3 ? $scoring["Customer_Relationships"] : "",
            'cr4'                    => $scoring["Customer_Relationships"] == 4 ? $scoring["Customer_Relationships"] : "",
            'cr5'                    => $scoring["Customer_Relationships"] == 5 ? $scoring["Customer_Relationships"] : "",

            'ci1'                    => $scoring["Continuous_Improvement"] == 1 ? $scoring["Continuous_Improvement"] : "",
            'ci2'                    => $scoring["Continuous_Improvement"] == 2 ? $scoring["Continuous_Improvement"] : "",
            'ci3'                    => $scoring["Continuous_Improvement"] == 3 ? $scoring["Continuous_Improvement"] : "",
            'ci4'                    => $scoring["Continuous_Improvement"] == 4 ? $scoring["Continuous_Improvement"] : "",
            'ci5'                    => $scoring["Continuous_Improvement"] == 5 ? $scoring["Continuous_Improvement"] : "",

            'la1'                    => $scoring["Learning_Agility"] == 1 ? $scoring["Learning_Agility"] : "",
            'la2'                    => $scoring["Learning_Agility"] == 2 ? $scoring["Learning_Agility"] : "",
            'la3'                    => $scoring["Learning_Agility"] == 3 ? $scoring["Learning_Agility"] : "",
            'la4'                    => $scoring["Learning_Agility"] == 4 ? $scoring["Learning_Agility"] : "",
            'la5'                    => $scoring["Learning_Agility"] == 5 ? $scoring["Learning_Agility"] : "",

            'ds1'                    => $scoring["Digital_Savvy"] == 1 ? $scoring["Digital_Savvy"] : "",
            'ds2'                    => $scoring["Digital_Savvy"] == 2 ? $scoring["Digital_Savvy"] : "",
            'ds3'                    => $scoring["Digital_Savvy"] == 3 ? $scoring["Digital_Savvy"] : "",
            'ds4'                    => $scoring["Digital_Savvy"] == 4 ? $scoring["Digital_Savvy"] : "",
            'ds5'                    => $scoring["Digital_Savvy"] == 5 ? $scoring["Digital_Savvy"] : "",
        ];

        $rata_rata = round((array_sum($personal_quality) / 14), 2);

        $rekomendasi_kriteria;
        if ($rata_rata < 2.58) {
            $rekomendasi_kriteria = "Tidak Disarankan";
        } elseif ($rata_rata >= 2.58 && $rata_rata < 2.82) {
            $rekomendasi_kriteria = "Disarankan dengan Pertimbangan";
        } else {
            $rekomendasi_kriteria = "Disarankan";
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('http://localhost/report_online/resources/template/olas/Online_Assessment_Report_OLAS_v3.docx');

        $getdata = $this->Assessment_model->getdatareport($this->post());
        
        $coverdata = [
            'no_participant'        => $getdata["participant"]->participant_number,
            'participant_name'      => $getdata["participant"]->participant_name,
            'participant_position'  => $getdata["participant"]->position,
            'participant_gelar_jurfak' => $getdata["participant"]->participant_gelar_jurfak,
            'participant_birth'     => "Jakarta" . "," . date_format(date_create($getdata["participant"]->created_at), 'd') . " " . convmont(date_format(date_create($getdata["participant"]->created_at), 'F')) . " " . date_format(date_create($getdata["participant"]->created_at), 'Y'),
            'assessment_date'       => date_format(date_create($getdata["participant"]->created_at), 'd') . " " . convmont(date_format(date_create($getdata["participant"]->created_at), 'F')) . " " . date_format(date_create($getdata["participant"]->created_at), 'Y'),
            'date_report'           => date("d ") . convmont(date("F")) . date(" Y "),
            'date_pengesahan'       => date("d ") . convmont(date("F")) . date(" Y "),
            'assessor_name'         => $getdata["assessor"]->fullname . ".," .  $getdata["assessor"]->assessor_degree,
            'assessor_degree'       => $getdata["assessor"]->assessor_degree,
            'assessor_sipp'         => substr($getdata["assessor"]->assessor_sipp, 0, 4) . "-" . substr($getdata["assessor"]->assessor_sipp, 4, 2) . "-" . substr($getdata["assessor"]->assessor_sipp, 6, 1) . "-" . substr($getdata["assessor"]->assessor_sipp, 7, 1),
            'qc_name'               => $getdata["qc"]->fullname . ".," . $getdata["qc"]->qc_degree,
            'qc_degree'             => $getdata["qc"]->qc_degree,
            'qc_sipp'               => substr($getdata["qc"]->qc_sipp, 0, 4) . "-" . substr($getdata["qc"]->qc_sipp, 4, 2) . "-" . substr($getdata["qc"]->qc_sipp, 6, 1) . "-" . substr($getdata["qc"]->qc_sipp, 7, 1),
            'desc_kekuatan'         => implode(". ", $descarray_kekuatan),
            'desc_pengembangan'     => implode(". ", $descarray_pengembang),

            'a1'                    => $scoring["Authenticity"] == 1 ? "X" : "",
            'a2'                    => $scoring["Authenticity"] == 2 ? "X" : "",
            'a3'                    => $scoring["Authenticity"] == 3 ? "X" : "",
            'a4'                    => $scoring["Authenticity"] == 4 ? "X" : "",
            'a5'                    => $scoring["Authenticity"] == 5 ? "X" : "",

            'ag1'                    => $scoring["Agility"] == 1 ? "X" : "",
            'ag2'                    => $scoring["Agility"] == 2 ? "X" : "",
            'ag3'                    => $scoring["Agility"] == 3 ? "X" : "",
            'ag4'                    => $scoring["Agility"] == 4 ? "X" : "",
            'ag5'                    => $scoring["Agility"] == 5 ? "X" : "",

            'ao1'                    => $scoring["Achievement_Orientation"] == 1 ? "X" : "",
            'ao2'                    => $scoring["Achievement_Orientation"] == 2 ? "X" : "",
            'ao3'                    => $scoring["Achievement_Orientation"] == 3 ? "X" : "",
            'ao4'                    => $scoring["Achievement_Orientation"] == 4 ? "X" : "",
            'ao5'                    => $scoring["Achievement_Orientation"] == 5 ? "X" : "",

            'cl1'                    => $scoring["Continuous_Learning"] == 1 ? "X" : "",
            'cl2'                    => $scoring["Continuous_Learning"] == 2 ? "X" : "",
            'cl3'                    => $scoring["Continuous_Learning"] == 3 ? "X" : "",
            'cl4'                    => $scoring["Continuous_Learning"] == 4 ? "X" : "",
            'cl5'                    => $scoring["Continuous_Learning"] == 5 ? "X" : "",

            'fc1'                    => $scoring["Fostering_Collaboration"] == 1 ? "X" : "",
            'fc2'                    => $scoring["Fostering_Collaboration"] == 2 ? "X" : "",
            'fc3'                    => $scoring["Fostering_Collaboration"] == 3 ? "X" : "",
            'fc4'                    => $scoring["Fostering_Collaboration"] == 4 ? "X" : "",
            'fc5'                    => $scoring["Fostering_Collaboration"] == 5 ? "X" : "",

            'dp1'                    => $scoring["Developing_Partnerships"] == 1 ? "X" : "",
            'dp2'                    => $scoring["Developing_Partnerships"] == 2 ? "X" : "",
            'dp3'                    => $scoring["Developing_Partnerships"] == 3 ? "X" : "",
            'dp4'                    => $scoring["Developing_Partnerships"] == 4 ? "X" : "",
            'dp5'                    => $scoring["Developing_Partnerships"] == 5 ? "X" : "",

            'dt1'                    => $scoring["Developing_Talent"] == 1 ? "X" : "",
            'dt2'                    => $scoring["Developing_Talent"] == 2 ? "X" : "",
            'dt3'                    => $scoring["Developing_Talent"] == 3 ? "X" : "",
            'dt4'                    => $scoring["Developing_Talent"] == 4 ? "X" : "",
            'dt5'                    => $scoring["Developing_Talent"] == 5 ? "X" : "",

            'ch1'                    => $scoring["Facilitating_Change"] == 1 ? "X" : "",
            'ch2'                    => $scoring["Facilitating_Change"] == 2 ? "X" : "",
            'ch3'                    => $scoring["Facilitating_Change"] == 3 ? "X" : "",
            'ch4'                    => $scoring["Facilitating_Change"] == 4 ? "X" : "",
            'ch5'                    => $scoring["Facilitating_Change"] == 5 ? "X" : "",

            'lt1'                    => $scoring["Leading_Teams"] == 1 ? "X" : "",
            'lt2'                    => $scoring["Leading_Teams"] == 2 ? "X" : "",
            'lt3'                    => $scoring["Leading_Teams"] == 3 ? "X" : "",
            'lt4'                    => $scoring["Leading_Teams"] == 4 ? "X" : "",
            'lt5'                    => $scoring["Leading_Teams"] == 5 ? "X" : "",

            'ca1'                    => $scoring["Commercial_Acumen"] == 1 ? "X" : "",
            'ca2'                    => $scoring["Commercial_Acumen"] == 2 ? "X" : "",
            'ca3'                    => $scoring["Commercial_Acumen"] == 3 ? "X" : "",
            'ca4'                    => $scoring["Commercial_Acumen"] == 4 ? "X" : "",
            'ca5'                    => $scoring["Commercial_Acumen"] == 5 ? "X" : "",

            'od1'                    => $scoring["Operational_Decision_Making"] == 1 ? "X" : "",
            'od2'                    => $scoring["Operational_Decision_Making"] == 2 ? "X" : "",
            'od3'                    => $scoring["Operational_Decision_Making"] == 3 ? "X" : "",
            'od4'                    => $scoring["Operational_Decision_Making"] == 4 ? "X" : "",
            'od5'                    => $scoring["Operational_Decision_Making"] == 5 ? "X" : "",

            'po1'                    => $scoring[0] == 1 ? "X" : "",
            'po2'                    => $scoring[0] == 2 ? "X" : "",
            'po3'                    => $scoring[0] == 3 ? "X" : "",
            'po4'                    => $scoring[0] == 4 ? "X" : "",
            'po5'                    => $scoring[0] == 5 ? "X" : "",

            'cr1'                    => $scoring["Customer_Relationships"] == 1 ? "X" : "",
            'cr2'                    => $scoring["Customer_Relationships"] == 2 ? "X" : "",
            'cr3'                    => $scoring["Customer_Relationships"] == 3 ? "X" : "",
            'cr4'                    => $scoring["Customer_Relationships"] == 4 ? "X" : "",
            'cr5'                    => $scoring["Customer_Relationships"] == 5 ? "X" : "",

            'ci1'                    => $scoring["Continuous_Improvement"] == 1 ? "X" : "",
            'ci2'                    => $scoring["Continuous_Improvement"] == 2 ? "X" : "",
            'ci3'                    => $scoring["Continuous_Improvement"] == 3 ? "X" : "",
            'ci4'                    => $scoring["Continuous_Improvement"] == 4 ? "X" : "",
            'ci5'                    => $scoring["Continuous_Improvement"] == 5 ? "X" : "",

            'la1'                    => $scoring["Learning_Agility"] == 1 ? "X" : "",
            'la2'                    => $scoring["Learning_Agility"] == 2 ? "X" : "",
            'la3'                    => $scoring["Learning_Agility"] == 3 ? "X" : "",
            'la4'                    => $scoring["Learning_Agility"] == 4 ? "X" : "",
            'la5'                    => $scoring["Learning_Agility"] == 5 ? "X" : "",

            'ds1'                    => $scoring["Digital_Savvy"] == 1 ? "X" : "",
            'ds2'                    => $scoring["Digital_Savvy"] == 2 ? "X" : "",
            'ds3'                    => $scoring["Digital_Savvy"] == 3 ? "X" : "",
            'ds4'                    => $scoring["Digital_Savvy"] == 4 ? "X" : "",
            'ds5'                    => $scoring["Digital_Savvy"] == 5 ? "X" : "",

            'rata_rata'              => $rata_rata,
            'rekomendasi_kriteria'   => $rekomendasi_kriteria,

            'atc'                    => $scoring["Authenticity"],
            'agy'                    => $scoring["Agility"],
            'acho'                   => $scoring["Achievement_Orientation"],
            'clearn'                 => $scoring["Continuous_Learning"],
            'fost'                   => $scoring["Fostering_Collaboration"],
            'devp'                   => $scoring["Developing_Partnerships"],
            'devtl'                  => $scoring["Developing_Talent"],
            'fac'                    => $scoring["Facilitating_Change"],
            'lead'                   => $scoring["Leading_Teams"],
            'comm'                   => $scoring["Commercial_Acumen"],
            'odesc'                  => $scoring["Operational_Decision_Making"],
            'plan'                   => $scoring[0],
            'crel'                   => $scoring["Customer_Relationships"],
            'imp'                    => $scoring["Continuous_Improvement"],
            'lagy'                   => $scoring["Learning_Agility"],
            'dgs'                    => 0,

            'pkau'                   => $scoring["Authenticity"],
            'pkag'                   => $scoring["Agility"],
            'pkao'                   => $scoring["Achievement_Orientation"],
            'pkcl'                   => $scoring["Continuous_Learning"],
            'pkfc'                   => $scoring["Fostering_Collaboration"],
            'pkdp'                   => $scoring["Developing_Partnerships"],
            'pkdt'                   => $scoring["Developing_Talent"],
            'pkfc'                   => $scoring["Facilitating_Change"],
            'pklt'                   => $scoring["Leading_Teams"],
            'pkca'                   => $scoring["Commercial_Acumen"],
            'pklt'                   => $scoring["Leading_Teams"],
            'pkod'                   => $scoring["Operational_Decision_Making"],
            'pkpo'                   => $scoring[0],
            'pkcr'                   => $scoring["Customer_Relationships"],
            'pkci'                   => $scoring["Continuous_Improvement"],
            'pkla'                   => $scoring["Learning_Agility"],
            'pkds'                   => $scoring["Digital_Savvy"],

            'tma1'                     => $scoring["Authenticity"] == 1 ? $scoring["Authenticity"] : "",
            'tma2'                     => $scoring["Authenticity"] == 2 ? $scoring["Authenticity"] : "",
            'tma3'                     => $scoring["Authenticity"] == 3 ? $scoring["Authenticity"] : "",
            'tma4'                     => $scoring["Authenticity"] == 4 ? $scoring["Authenticity"] : "",
            'tma5'                     => $scoring["Authenticity"] == 5 ? $scoring["Authenticity"] : "",

            'tmag1'                    => $scoring["Agility"] == 1 ? $scoring["Agility"] : "",
            'tmag2'                    => $scoring["Agility"] == 2 ? $scoring["Agility"] : "",
            'tmag3'                    => $scoring["Agility"] == 3 ? $scoring["Agility"] : "",
            'tmag4'                    => $scoring["Agility"] == 4 ? $scoring["Agility"] : "",
            'tmag5'                    => $scoring["Agility"] == 5 ? $scoring["Agility"] : "",

            'tmdp1'                    => $scoring["Developing_Partnerships"] == 1 ? $scoring["Developing_Partnerships"] : "",
            'tmdp2'                    => $scoring["Developing_Partnerships"] == 2 ? $scoring["Developing_Partnerships"] : "",
            'tmdp3'                    => $scoring["Developing_Partnerships"] == 3 ? $scoring["Developing_Partnerships"] : "",
            'tmdp4'                    => $scoring["Developing_Partnerships"] == 4 ? $scoring["Developing_Partnerships"] : "",
            'tmdp5'                    => $scoring["Developing_Partnerships"] == 5 ? $scoring["Developing_Partnerships"] : "",

            'tmdt1'                    => $scoring["Developing_Talent"] == 1 ? $scoring["Developing_Talent"] : "",
            'tmdt2'                    => $scoring["Developing_Talent"] == 2 ? $scoring["Developing_Talent"] : "",
            'tmdt3'                    => $scoring["Developing_Talent"] == 3 ? $scoring["Developing_Talent"] : "",
            'tmdt4'                    => $scoring["Developing_Talent"] == 4 ? $scoring["Developing_Talent"] : "",
            'tmdt5'                    => $scoring["Developing_Talent"] == 5 ? $scoring["Developing_Talent"] : "",

            'tmca1'                    => $scoring["Commercial_Acumen"] == 1 ? $scoring["Commercial_Acumen"] : "",
            'tmca2'                    => $scoring["Commercial_Acumen"] == 2 ? $scoring["Commercial_Acumen"] : "",
            'tmca3'                    => $scoring["Commercial_Acumen"] == 3 ? $scoring["Commercial_Acumen"] : "",
            'tmca4'                    => $scoring["Commercial_Acumen"] == 4 ? $scoring["Commercial_Acumen"] : "",
            'tmca5'                    => $scoring["Commercial_Acumen"] == 5 ? $scoring["Commercial_Acumen"] : "",

            'tmod1'                    => $scoring["Operational_Decision_Making"] == 1 ? $scoring["Operational_Decision_Making"] : "",
            'tmod2'                    => $scoring["Operational_Decision_Making"] == 2 ? $scoring["Operational_Decision_Making"] : "",
            'tmod3'                    => $scoring["Operational_Decision_Making"] == 3 ? $scoring["Operational_Decision_Making"] : "",
            'tmod4'                    => $scoring["Operational_Decision_Making"] == 4 ? $scoring["Operational_Decision_Making"] : "",
            'tmod5'                    => $scoring["Operational_Decision_Making"] == 5 ? $scoring["Operational_Decision_Making"] : "",

            'tmpo1'                    => $scoring[0] == 1 ? $scoring[0] : "",
            'tmpo2'                    => $scoring[0] == 2 ? $scoring[0] : "",
            'tmpo3'                    => $scoring[0] == 3 ? $scoring[0] : "",
            'tmpo4'                    => $scoring[0] == 4 ? $scoring[0] : "",
            'tmpo5'                    => $scoring[0] == 5 ? $scoring[0] : "",

            'tmcr1'                    => $scoring["Customer_Relationships"] == 1 ? $scoring["Customer_Relationships"] : "",
            'tmcr2'                    => $scoring["Customer_Relationships"] == 2 ? $scoring["Customer_Relationships"] : "",
            'tmcr3'                    => $scoring["Customer_Relationships"] == 3 ? $scoring["Customer_Relationships"] : "",
            'tmcr4'                    => $scoring["Customer_Relationships"] == 4 ? $scoring["Customer_Relationships"] : "",
            'tmcr5'                    => $scoring["Customer_Relationships"] == 5 ? $scoring["Customer_Relationships"] : "",

            'tmci1'                    => $scoring["Continuous_Improvement"] == 1 ? $scoring["Continuous_Improvement"] : "",
            'tmci2'                    => $scoring["Continuous_Improvement"] == 2 ? $scoring["Continuous_Improvement"] : "",
            'tmci3'                    => $scoring["Continuous_Improvement"] == 3 ? $scoring["Continuous_Improvement"] : "",
            'tmci4'                    => $scoring["Continuous_Improvement"] == 4 ? $scoring["Continuous_Improvement"] : "",
            'tmci5'                    => $scoring["Continuous_Improvement"] == 5 ? $scoring["Continuous_Improvement"] : "",

            'tmla1'                    => $scoring["Learning_Agility"] == 1 ? $scoring["Learning_Agility"] : "",
            'tmla2'                    => $scoring["Learning_Agility"] == 2 ? $scoring["Learning_Agility"] : "",
            'tmla3'                    => $scoring["Learning_Agility"] == 3 ? $scoring["Learning_Agility"] : "",
            'tmla4'                    => $scoring["Learning_Agility"] == 4 ? $scoring["Learning_Agility"] : "",
            'tmla5'                    => $scoring["Learning_Agility"] == 5 ? $scoring["Learning_Agility"] : "",

            'tmds1'                    => $scoring["Digital_Savvy"] == 1 ? $scoring["Digital_Savvy"] : "",
            'tmds2'                    => $scoring["Digital_Savvy"] == 2 ? $scoring["Digital_Savvy"] : "",
            'tmds3'                    => $scoring["Digital_Savvy"] == 3 ? $scoring["Digital_Savvy"] : "",
            'tmds4'                    => $scoring["Digital_Savvy"] == 4 ? $scoring["Digital_Savvy"] : "",
            'tmds5'                    => $scoring["Digital_Savvy"] == 5 ? $scoring["Digital_Savvy"] : "",
        ];

        $path = 'resources/report/';
        $filename_report = "Assessment_report_olas_" . str_replace(" ", "_", $coverdata["participant_name"]);

        $saving_dt = [
            "participant_id" => $this->post("participant_id"),
            "report_name"    => $filename_report . ".docx",
            "report_generate"=> json_encode($coverdata),
            "created_by"     => $this->session->userdata("users")["fullname"],
            "created_at"     => date("Y-m-d H:i:s")
        ];
        
        $templateProcessor->setValues($coverdata);
        
        $i = 1;
        foreach ($final_area_kekuatan as $key => $value) {
            $templateProcessor->setValue("competences#".$i, $key);
            foreach ($value["list_statement"] as $k2 => $val) {
                $templateProcessor->setValue("list_statement#".$i."#".($k2+1), $val);
            }
            $i++;
        }

        $dtpr_up = [
            "status_report" => 1
        ];

        $dtas_up = [
            "status_report" => "finished",
            "rp_finish_at"  => date("Y-m-d H:i:s")
        ];
        
        $this->db->where("id", $this->post("participant_id"))->update("participant", $dtpr_up);
        $this->db->where("user_id", $this->session->userdata("users")["id"])->update("assessor", $dtas_up);

        $this->db->insert("master_reports", $saving_dt);
        $templateProcessor->saveAs($path.$filename_report.".docx", 'php://output');

        insertLog("Report Assessment " . $getdata["participant"]->participant_name, base_url() . $this->uri->segment(1));
        redirect("/");
    }

    // GENERATE WORD
    public function generatebytemplate_post()
    {
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('http://localhost/report_online/resources/template/tes.docx');

        $coverdata = [
            'participant_nopek'     => "oy",
        ];

        $templateProcessor->setValues($coverdata);
        
        header("Content-Disposition: attachment; filename=olas.docx");
        
        $templateProcessor->saveAs('php://output');
    }

    public function generateword_post()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $this->coverletter($section, $phpWord);

        $this->pengesahan($section, $phpWord);

        $this->pengantar($section, $phpWord);

        $this->pengantartwo($section, $phpWord);
    
        $writer = new Word2007($phpWord);
		
		$filename = 'simple';
		
		header('Content-Type: application/msword');
        header('Content-Disposition: attachment;filename="'. $filename .'.docx"'); 
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output');
    }

    public function coverletter($section, $phpWord)
    {
        $phpWord->getSettings()->setEvenAndOddHeaders(true);
        $section = $phpWord->addSection();

        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(true);
        $fontStyle->setName('Arial');
        $fontStyle->setSize(36);

        $header = $section->addHeader();

        $header->addImage('http://localhost/report_online/assets/web/img/cover-header-flip.png',
            array(
                'width'=>600,
                'height'=>140,
                'positioning' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posHorizontal' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posVertical' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'marginLeft' => -75,
                'marginTop'=>-50,
                'wrappingStyle'=> 'behind'
            ));

        $section->addText("");
        $section->addText("");
        $section->addText("");
        
        $section->addImage(
            'http://localhost/report_online/assets/web/img/pertamina-logo.png',
            array(
                'width'         => 180,
                'height'        => 44,
            )
        );
        
        $section->addText("");
        $section->addText("");

		$section->addText('LAPORAN HASIL')->setFontStyle($fontStyle);

        $section->addText('Operation Leader Assessment Survey (OLAS)')
                ->setFontStyle(
                    array(
                        'bold'    => true,
                        'name'    => "Cambria",
                        'size'    => "20",
                        'italic'  => true,
                        'color'   => "#2980b9"
                    )
                );

        // ENTER 4
        $section->addText("");
        $section->addText("");

        $section->addText(strtoupper($this->post("participant_name")))
                ->setFontStyle(
                    array(
                        'bold'    => true,
                        'name'    => "Cambria",
                        'size'    => "24",
                        'color'   => "#2980b9"
                    )
                );

        $section->addText(date("d F Y"))
                ->setFontStyle(
                    array(
                        'bold'    => true,
                        'name'    => "Cambria",
                        'size'    => "16",
                    )
                );

        $section->addText("");
        $section->addText("");

        $section->addText('Laporan ini bersifat rahasia, dibuat untuk PT Pertamina (Persero).')
                ->setFontStyle(
                    array(
                        'name'    => "Arial",
                        'size'    => "9",
                        'italic'  => true,
                    )
                );

        $section->addText('Segala tindak lanjut dari laporan ini adalah wewenang')
                ->setFontStyle(
                    array(
                        'name'    => "Arial",
                        'size'    => "9",
                        'italic'  => true,
                    )
                );

        $section->addText('PT Pertamina (Persero).')
                ->setFontStyle(
                    array(
                        'name'    => "Arial",
                        'size'    => "9",
                        'italic'  => true,
                    )
                );

        $section->addText("");
        $section->addText("");

        $ptctext = $section->addText('PT Pertamina Training Consulting.');
        $ptctext->setFontStyle(
                    array(
                        'name'    => "Arial",
                        'size'    => "9",
                        'bold'  => true,
                    )
                );
        $ptctext->setParagraphStyle(
            array(
                'alignment' => "right",
            )
        );

        $footer = $section->addFooter();
        $footer->addImage('http://localhost/report_online/assets/web/img/cover-footer.png',
            array(
                'width'=>603,
                'height'=>71,
                'positioning' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posHorizontal' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'posVertical' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
                'marginLeft' => -75,
                'marginTop'=>-10,
                'wrappingStyle'=> 'behind'
            ));
    }

    public function pengesahan($section, $phpWord)
    {
        $section = $phpWord->addSection(array('pageNumberingStart' => 1));
        $header = $section->addHeader();
        $table = $header->addTable();
        $table->addRow();

        $cell = $table->addCell(14000)->addImage('http://localhost/report_online/assets/web/img/pertamina-logo.png',
                        array('width' => 123, 'height' => 30, 'align' => 'right', 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::LEFT)
        );
            
        $cell3 = $table->addCell(14000)->addText("CONFIDENTAL", null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT))->setFontStyle(
            array(
                'bold'    => true,
                'name'    => "Arial",
                'size'    => "11",
                'italic'  => true,
                'color'   => "#000",
        ));

        // ENTER 6
        $section->addText("");
        $section->addText("");
        $section->addText("");

        $myTextElement  = $section->addText('Laporan ini berbasis pada metode asesmen dengan menggunakan multi-tools.');
        $myTextElement->setFontStyle(
            array(
                'name'    => "Arial",
                'size'    => "11",
                'color'   => "#000",
            )
        );
        $myTextElement->setParagraphStyle(
                                            array(
                                                'alignment' => "center",
                                                'lineHeight' => 1.15
                                            )
                                        );

        $myTextElement2 = $section->addText('Sebagai parameter kompetensi, digunakan model kompetensi');
        $myTextElement2->setFontStyle(
            array(
                'name'    => "Arial",
                'size'    => "11",
                'color'   => "#000",
            )
        );
        $myTextElement2->setParagraphStyle(
                                            array(
                                                'alignment' => "center",
                                                'lineHeight' => 1.15,
                                                "spaceBefore" => 1,
                                            )
                                        );

        $myTextElement3 = $section->addText('yang berlaku di PT Pertamina (Persero).');
        $myTextElement3->setFontStyle(
            array(
                'name'    => "Arial",
                'size'    => "11",
                'color'   => "#000",
            )
        );
        $myTextElement3->setParagraphStyle(
                                            array(
                                                'alignment' => "center",
                                                'lineHeight' => 1.15,
                                                "spaceBefore" => 1,
                                            )
                                        );

        $section->addText("");
        $section->addText("");

        $section->addText('', $header);
        
        $fancyTableStyle = array('borderSize' => 0, 'borderColor' => 'FFFFFF');
        $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center', 'bgColor' => 'FFFF00');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 3, 'valign' => 'center');
        $cellHCentered = array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
        $cellVCentered = array('valign' => 'center');
        
        $spanTableStyleName = 'Colspan Rowspan';
        $phpWord->addTableStyle($spanTableStyleName, $fancyTableStyle);
        $tablep2 = $section->addTable($spanTableStyleName);
        
        $tablep2->addRow();
        $cell2 = $tablep2->addCell(12000, $cellColSpan);
        $textrun2 = $cell2->addTextRun($cellHCentered);
        $textrun2->addText('Jakarta, ' . date("F Y"));

        $tablep2->addRow();
        $cell3 = $tablep2->addCell(12000, $cellColSpan);
        $textrun3 = $cell3->addTextRun($cellHCentered);
        $textrun3->addText('');

        $tablep2->addRow();
        $cell4 = $tablep2->addCell(12000, $cellColSpan);
        $textrun4 = $cell4->addTextRun($cellHCentered);
        $textrun4->addText('');
        
        $tablep2->addRow();
        $tablep2->addCell(null, $cellRowContinue);
        $tablep2->addCell(8000, $cellVCentered)->addText('Assessor', null, $cellHCentered);
        $tablep2->addCell(8000, $cellVCentered)->addText('Quality Control', null, $cellHCentered);
        $tablep2->addCell(null, $cellRowContinue);

        $tablep2->addRow();
        $tablep2->addCell(null, $cellRowContinue);
        $tablep2->addCell(8000, $cellVCentered)->addText('', null, $cellHCentered);
        $tablep2->addCell(8000, $cellVCentered)->addText('', null, $cellHCentered);
        $tablep2->addCell(null, $cellRowContinue);

        $tablep2->addRow();
        $tablep2->addCell(null, $cellRowContinue);
        $tablep2->addCell(8000, $cellVCentered)->addText('', null, $cellHCentered);
        $tablep2->addCell(8000, $cellVCentered)->addText('', null, $cellHCentered);
        $tablep2->addCell(null, $cellRowContinue);

        $tablep2->addRow();
        $tablep2->addCell(null, $cellRowContinue);
        $tablep2->addCell(8000, $cellVCentered)->addText('Assesor Name S.Psi', null, $cellHCentered);
        $tablep2->addCell(8000, $cellVCentered)->addText('Quality Control S.Psi', null, $cellHCentered);
        $tablep2->addCell(null, $cellRowContinue);

        $tablep2->addRow();
        $tablep2->addCell(null, $cellRowContinue);
        $tablep2->addCell(8000, $cellVCentered)->addText('SIPP : 0303-03-0-3', null, $cellHCentered);
        $tablep2->addCell(8000, $cellVCentered)->addText('SIPP : 1111-11-1-1', null, $cellHCentered);
        $tablep2->addCell(null, $cellRowContinue);

        //================= FOOTER ========
        $footerpage = $section->addFooter();
        $tablepage = $footerpage->addTable();
        $tablepage->addRow();

        $cellpage = $tablepage->addCell(14000)->addText($this->post("participant_name"), null, array(
            'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::LEFT
        ))->setFontStyle(
            array(
                'name'    => "Arial",
                'italic'  => true,
                'size'    => "8",
                'color'   => "#000",
            ));

        $cellpage2 = $tablepage->addCell(14000)->addText("c 2020 PT Pertamina Training Consulting", null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER))->setFontStyle(
            array(
                'name'    => "Arial",
                'size'    => "8",
                'italic'  => true,
                'color'   => "#000",
            ));
            
        $cellpage3 = $tablepage->addCell(14000)->addText("", null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT))->setFontStyle(
            array(
                'name'    => "Arial",
                'size'    => "8",
                'italic'  => false,
                'color'   => "#000",
            ));
  
    }

    public function pengantar($section, $phpWord)
    {
        $section->addPageBreak();
        $phpWord->getSettings()->setEvenAndOddHeaders(false);
        $phpWord->setDefaultParagraphStyle(
            array(
                'alignment'  => \PhpOffice\PhpWord\SimpleType\Jc::BOTH,
                'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(12),
                'spacing'    => 120,
                'lineHeight' => 1.15
            )
        );
        $section = $phpWord->addSection();
        $section->addText("PENGANTAR", 
            array(
                "name"  => "Cambria",
                "size"  =>  "22",
                "bold"  =>  true,
                "color" =>  "#2980b9"
            )
        );

        $fontpengantar = array(
            "name" => "Arial", 
            "size" => "11",
        );

        $textrun = $section->addTextRun();
        $textrun->addText(
            'Laporan ini merupakan laporan individual yang bertujuan untuk memberikan gambaran mengenai profil kompetensi yang dipersyaratkan. Profil kompetensi individu didapatkan dari hasil penilaian melalui metode ', 
            $fontpengantar
        );

        $textrun->addText(
            'Online Assessment.', 
            array_merge($fontpengantar, array("italic" => true))
        );

        $textrun2 = $section->addTextRun();
        $textrun2->addText(
            'Online Assessment',
            array_merge($fontpengantar, array("italic" => true))
        );

        $textrun2->addText(
            ' adalah metode pengukuran kompetensi yang dilakukan dalam jaringan (online) dengan mengacu pada profil kompetensi yang dijadikan sebagai parameter. Kompetensi memberi gambaran mengenai segala sesuatu yang mampu dilakukan oleh seorang peserta. Kompetensi berbasis pada perilaku memudahkan proses pengukuran sehingga dapat dijadikan panduan baik dalam proses seleksi maupun sebagai bahan pertimbangan dalam memberikan keputusan pengelolaan pekerja.',
            $fontpengantar
        );

        $textrun2 = $section->addTextRun();
        $textrun2->addText(
            'Untuk mendapatkan data mengenai kompetensi yang dimaksud, digunakan teknik-teknik pengukuran berbasis kompetensi perilaku yaitu simulasi dan wawancara.',
            $fontpengantar
        );

        $section->addText("");

        $section->addText("Metode dan Alat (Tools) yang Digunakan", 
            array(
                "name"  => "Cambria",
                "size"  =>  "14",
                "bold"  =>  true,
                "color" =>  "#2980b9",
                "italic"=> true
            )
        );

        $textrun3 = $section->addTextRun();
        $textrun3->addText(
            'Selama asesmen, peserta terlibat dalam beberapa kegiatan online sebagai berikut:'
        );

        // Define styles
        $fontStyleName = 'myOwnStyle';
        $phpWord->addFontStyle($fontStyleName, array(
            'color' => '000000', 
            'bold'  => true,
            'italic'=> true
        ));

        $paragraphStyleName = 'P-Style';
        $indentStyle = new \PhpOffice\PhpWord\Style\Indentation;
        $phpWord->addParagraphStyle($paragraphStyleName, array(
            'lineHeight' => 1.15,
        ));

        $predefinedMultilevelStyle = array('listType' => \PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER);
        $section->addListItem('Personality Questionnaire', 0, $fontStyleName, $predefinedMultilevelStyle, array('indentation' => array('left' => 0, 'right' => 0, 'hanging' => 10), 'lineHeight' => 1.15));
        $section->addText("Peserta diminta untuk mengerjakan test kepribadian secara online, dengan cara memilih jawaban yang sesuai dengan pribadinya dalam menghadapi situasi yang dijumpai di lingkungan kerjanya.", 
            null,
            array(
                'indentation' => array('left' => 20, 'right' => 0, 'hanging' => 10)
            )
        );
        
        $section->addListItem('Analisis Kasus', 0, $fontStyleName, $predefinedMultilevelStyle, array('indentation' => array('left' => 0, 'right' => 0, 'hanging' => 10), 'lineHeight' => 1.15));
        $section->addText("Peserta diminta membuat analisa dan menyusun rekomendasi atas permasalahan organisasi yang diberikan. Peserta diperkenankan memanfaatkan berbagai pendekatan atau metode analisa yang dipahaminya.", 
            null,
            array(
                'indentation' => array('left' => 20, 'right' => 0, 'hanging' => 10)
            )
        );

        $section->addListItem('Critical Incident', 0, $fontStyleName, $predefinedMultilevelStyle, array('indentation' => array('left' => 0, 'right' => 0, 'hanging' => 10), 'lineHeight' => 1.15));
        $section->addText("Peserta diminta untuk menuliskan pengalaman mengenai keberhasilan maupun situasi yang kurang sesuai dengan target yang diharapkan, yang pernah dihadapi dalam melaksanakan tugas dan tanggung jawab kerja selama kurun waktu 2 tahun terakhir.",
            array(
                'indentation' => array('left' => 20, 'right' => 0, 'hanging' => 10)
            )
        );

        $section->addText("");
        
        $section->addText("Sistem Penilaian", 
            array(
                "name"  => "Cambria",
                "size"  =>  "14",
                "bold"  =>  true,
                "color" =>  "#2980b9",
                "italic"=> true
            )
        );

        $section->addText("Kompetensi dinilai dengan mengacu pada model kompetensi Operation Leader Assessment Survey (OLAS) PT Pertamina (Persero).");
        $section->addText("Untuk membantu proses analisis, kompetensi dikelompokkan menjadi 4 (empat) kelompok kompetensi, yaitu:");
    }

    public function pengantartwo($section, $phpWord)
    {
        $section->addPageBreak();
        $phpWord->getSettings()->setEvenAndOddHeaders(false);
        $phpWord->setDefaultParagraphStyle(
            array(
                'alignment'  => \PhpOffice\PhpWord\SimpleType\Jc::BOTH,
                'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(12),
                'spacing'    => 120,
                'lineHeight' => 1.15
            )
        );

        $section = $phpWord->addSection();

        $section->addText("Personal Quality", array("italic" => true, "size" => 12));
        $listItemRun = $section->addListItemRun(0, array('listType' => \PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER), array('indentation' => array('left' => 0, 'right' => 0, 'hanging' => 10), 'lineHeight' => 1.15));
        $listItemRun->addText('Kelompok Kompetensi Intra Personal,', array('bold' => true));
        $listItemRun->addText(' yaitu kelompok kompetensi yang terkait dengan kemampuan individu dalam membangun dan mengelola diri. Kelompok ini meliputi ');
        $listItemRun->addText(' Authenticity, Agility, Achievement Orientation, dan Continuous Learning.', array("italic" => true));
    }
}
