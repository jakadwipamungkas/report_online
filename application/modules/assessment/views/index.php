<div class="main-content" ng-controller="assessment">
    <style>
        .select2-container--default .select2-selection--single {
            background-color: #fff !important;
            border: 1px solid #d7d7fc !important;
            border-radius: 4px !important;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            display: block;
            padding-left: 8px;
            padding-top: 6px;
            padding-right: 20px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="pills-cover-tab" data-toggle="pill"
                                            href="#pills-cover" role="tab" aria-controls="pills-cover"
                                            aria-selected="true">Cover Report</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="pills-ringkasanindividu-tab" data-toggle="pill"
                                            href="#pills-ringkasanindividu" role="tab"
                                            aria-controls="pills-ringkasanindividu" aria-selected="false">Ringkasan
                                            Individu</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="pills-formevaluasi-tab" data-toggle="pill"
                                            href="#pills-formevaluasi" role="tab" aria-controls="pills-formevaluasi"
                                            aria-selected="false">Form Evaluasi</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="pills-sp-tab" data-toggle="pill" href="#pills-sp"
                                            role="tab" aria-controls="pills-sp" aria-selected="false">Saran
                                            Pengembangan</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="<?= base_url("saving") ?>" method="POST">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-cover" role="tabpanel"
                            aria-labelledby="pills-cover-tab">
                            <div class="card p-4">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h5><b class="text-dark">LAPORAN HASIL</b></h5>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h6><i class="text-primary">Operation Leader Assessment Survey (OLAS)</i>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="card-header-action">

                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label>Assessor <span class="text-danger"> * </span></label>
                                                <input type="hidden" class="form-control" name="idassess" id="idassess"
                                                    value="<?= $users["id"] ?>" readonly>
                                                <input type="text" class="form-control" name="assessor" id="assessor"
                                                    value="<?= $users["fullname"] ?>" readonly>
                                            </div>

                                            <div class="form-group mandiri">
                                                <label>Participants Name <span class="text-danger"> * </span></label>
                                                <select class="form-control participant_name" name="participant_id"
                                                    id="participant_id" ng-model="assessreport.participant_id"
                                                    ng-change="setParticipant()">
                                                    <option value="" selected>-- Choose Participant --</option>
                                                    <option ng-repeat="(kp, vp) in dataParticipant"
                                                        ng-bind="vp.participant_name" value="{{vp.id}}"></option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Participants Code <span class="text-danger"> * </span></label>
                                                <input type="text" class="form-control" name="participants_code"
                                                    id="participants_code" ng-model="assessreport.participant_number">
                                            </div>

                                            <div class="form-group penugasan">
                                                <label>Participants Name <span class="text-danger"> * </span></label>
                                                <input type="text" class="form-control" name="participant_name"
                                                    id="participant_name" ng-model="assessreport.participant_name">
                                            </div>

                                            <div class="form-group penugasan">
                                                <label>Participants Position <span class="text-danger"> *
                                                    </span></label>
                                                <input type="text" class="form-control" name="participants_position"
                                                    id="participants_position"
                                                    ng-model="assessreport.participant_position">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header-action float-right">
                                        <!-- <a href="#" class="btn btn-info"><i class="fas fa-eye"></i> Preview</a> -->
                                        <!-- <button class="btn btn-primary" type="submit"><i class="fas fa-check-circle"></i> Save</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-ringkasanindividu" role="tabpanel"
                            aria-labelledby="pills-ringkasanindividu-tab">
                            <div class="card p-4">
                                <!-- Area Kekuatan -->
                                <!-- <form action="<?= base_url("save_individu") ?>" method="POST"> -->
                                <div class="card-header bg-primary"
                                    style="min-height: 0px !important; padding: 10px 25px !important;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h4><b class="text-white"><i>Area Kekuatan</i></b></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <p>
                                                <textarea type="text" style="height: 120px !important;"
                                                    class="form-control d-none" name="desc_kekuatan" id="desc_kekuatan"
                                                    ng-model="assessreport.area_kekuatan"></textarea>
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <div class="kekuatan"
                                                ng-repeat="(keyformassess, formassess) in multiformassess">
                                                <div class="form-group row">
                                                    <label for="behavior"
                                                        class="col-sm-2 col-form-label">Behaviors</label>
                                                    <div class="col-sm-6">
                                                        <select class="form-control behaviors"
                                                            name="behaviors_kekuatan[]" id="behaviors"
                                                            ng-model="formassess.behaviors" ng-change="setBehaviors()">
                                                            <option value="" selected>-- Choose Behaviors --</option>
                                                            <option ng-repeat="(kb, vb) in list_behavior"
                                                                class="disabled" ng-bind="vb.behavior_name"
                                                                value="{{vb.behavior_name}}"></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="competences"
                                                        class="col-sm-2 col-form-label">Competences</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control"
                                                            name="competences_kekuatan[]" id="competences"
                                                            ng-model="formassess.competences">
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="behavior" class="col-sm-2 col-form-label">Statement
                                                        <b>(+)</b></label>
                                                    <div class="col-sm-10">
                                                        <!-- <ul ng-repeat="vg in formassess.glossary.uraian_positif track by $index">
                                                                    <li>{{vg}}</li>
                                                                </ul> -->
                                                        <div class="custom-control custom-checkbox"
                                                            ng-repeat="vg in formassess.glossary.uraian_positif track by $index">
                                                            <!-- <div ng-repeat="vpo in vg.uraian_positif track by $index"> -->
                                                            <input type="checkbox"
                                                                name="{{formassess.behaviors}}[positif][]"
                                                                id="chk-{{vg}}" ng-value="vg">&nbsp; {{vg}}
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="behavior" class="col-sm-2 col-form-label">Statement
                                                        <b>(-)</b></label>
                                                    <div class="col-sm-10">
                                                        <div class="custom-control custom-checkbox"
                                                            ng-repeat="vne in formassess.glossary.uraian_negatif track by $index">
                                                            <!-- <div ng-repeat="(kne, vne) in vgn.uraian_negatif"> -->
                                                            <input type="checkbox"
                                                                name="{{formassess.behaviors}}[negatif][]"
                                                                id="chknegatif-{{vne}}" ng-value="vne">&nbsp; {{vne}}
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="behavior" class="col-sm-2 col-form-label"></label>
                                                    <div class="col-sm-10">
                                                        <a href="" class="btn btn-sm btn-danger float-right"
                                                            ng-click="removeColumn(keyformassess)"><i
                                                                class="fas fa-plus"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 col-md-4 col-lg-4">
                                            <button type="button" class="btn btn-success btn-add-crqnumber"
                                                ng-click="addRowkekuatan()"><i class="fas fa-plus"></i> Add
                                                More</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- Area Pengembangan -->
                                <div class="card-header bg-primary"
                                    style="min-height: 0px !important; padding: 10px 25px !important;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h4><b class="text-white"><i>Area Pengembangan</i></b></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <p>
                                                <textarea type="text" class="form-control d-none"
                                                    style="border: none !important;" name="desc_kekuatan"
                                                    id="desc_kekuatan"
                                                    ng-model="assessreport.area_pengembangan"></textarea>
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <div class="kekuatan"
                                                ng-repeat="(keyformpengembangan, formpengembangan) in multiformpengembangan">
                                                <div class="form-group row">
                                                    <label for="behavior"
                                                        class="col-sm-2 col-form-label">Behaviors</label>
                                                    <div class="col-sm-6">
                                                        <select class="form-control behaviors"
                                                            name="behaviors_pengembang[]" id="behaviors"
                                                            ng-model="formpengembangan.behaviors"
                                                            ng-change="setBehaviorsPengembangan()">
                                                            <option value="" selected>-- Choose Behaviors --</option>
                                                            <option
                                                                ng-repeat="(kb, vb) in list_behavior_area_pengembangan"
                                                                ng-bind="vb.behavior_name" value="{{vb.behavior_name}}">
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row d-none" ng-if="loadglossary == true">
                                                    <label for="competences"
                                                        class="col-sm-2 col-form-label">Competences</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control"
                                                            name="competences_pengembang[]" id="competences"
                                                            ng-model="formpengembangan.competences">
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="behavior" class="col-sm-2 col-form-label">Statement
                                                        <b>(+)</b></label>
                                                    <div class="col-sm-10">
                                                        <div class="custom-control custom-checkbox"
                                                            ng-repeat="(kpo, vpo) in formpengembangan.glossary.uraian_positif track by $index">
                                                            <!-- <div ng-repeat="(kpo, vpo) in vg.uraian_positif"> -->
                                                            <input type="checkbox"
                                                                name="areapengembangan-{{formpengembangan.behaviors}}[positif_pengembangan][]"
                                                                id="chk-{{vpo}}" ng-value="vpo">&nbsp; {{vpo}}
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="behavior" class="col-sm-2 col-form-label">Statement
                                                        <b>(-)</b></label>
                                                    <div class="col-sm-10">
                                                        <div class="custom-control custom-checkbox"
                                                            ng-repeat="vne in formpengembangan.glossary.uraian_negatif track by $index">
                                                            <!-- <div ng-repeat="(kne, vne) in vgn.uraian_negatif"> -->
                                                            <input type="checkbox"
                                                                name="areapengembangan-{{formpengembangan.behaviors}}[negatif_pengembangan][]"
                                                                id="chk-{{vne}}" ng-value="vne">&nbsp; {{vne}}
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" ng-if="loadglossary == true">
                                                    <label for="behavior" class="col-sm-2 col-form-label"></label>
                                                    <div class="col-sm-10">
                                                        <a href="" class="btn btn-sm btn-danger float-right"
                                                            ng-click="removeColumnPengembangan(keyformpengembangan)"><i
                                                                class="fas fa-plus"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 col-md-4 col-lg-4">
                                            <button type="button" class="btn btn-success btn-add-crqnumber"
                                                ng-click="addRowPengembangan()"><i class="fas fa-plus"></i> Add
                                                More</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <!-- <button type="submit" class="btn btn-primary float-right ml-1"><i class="fas fa-check-circle"></i> Submit</button> -->
                                    <!-- <a href="#" class="btn btn-info float-right"><i class="fas fa-eye"></i> Preview</a> -->
                                </div>
                                <!-- </form> -->
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-formevaluasi" role="tabpanel"
                            aria-labelledby="pills-formevaluasi-tab">
                            <div class="card p-4">
                                <div class="card-header"
                                    style="min-height: 0px !important; padding: 10px 25px !important;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h4><b><i>Form Evaluasi</i></b></h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- <form action="<?= base_url("save_evaluasi") ?>" method="POST"> -->
                                <div class="card-body">
                                    <div class="table-responsive" style="max-height:500px !important; overflow: auto;">
                                        <table class="table table-striped table-hover table-bordered mt-3"
                                            ng-repeat="(kval, vval) in listevaluasi">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2" class="text-left bg-white">
                                                        <span class="text-dark">{{kval}}</span><br>
                                                        <span class="text-dark">{{vval.desc}}</span>
                                                        </td>
                                                    <th colspan="3" rowspan="2" class="text-center bg-white text-dark">0
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <!-- <th colspan="3" class="bg-white text-white text-bold"><span class="badge badge-info">0</span></td> -->
                                                </tr>
                                                <tr>
                                                    <th>Perilaku Utama</th>
                                                    <th>Sangat Efektif</th>
                                                    <th>Efektif</th>
                                                    <th>Kurang</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="(kvior, vvior) in vval.behavior">
                                                    <td>
                                                        <span>{{vvior.behavior_name}}</span>
                                                    </td>
                                                    <td>
                                                        <div class="text-center">
                                                            <input type="checkbox" id="sangatefektif"
                                                                name="formevaluasi[{{vval.comp_nonspace}}][a_sangatefektif_{{kvior}}]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text_center">
                                                            <input type="checkbox" id="efektif"
                                                                name="formevaluasi[{{vval.comp_nonspace}}][b_efektif_{{kvior}}]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text_center">
                                                            <input type="checkbox" id="kurang"
                                                                name="formevaluasi[{{vval.comp_nonspace}}][c_kurang_{{kvior}}]">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <!-- <button class="btn btn-primary float-right" type="submit"><i class="fas fa-check-circle"></i> Save</button> -->
                                    <!-- <a href="#" class="btn btn-info float-right"><i class="fas fa-eye"></i> Preview</a> -->
                                </div>
                                <!-- </form> -->
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-sp" role="tabpanel" aria-labelledby="pills-sp-tab">
                            <div class="card p-4">
                                <div class="card-header"
                                    style="min-height: 0px !important; padding: 10px 25px !important;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <h4><b><i>Saran Pengembangan</i></b></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label for="competences">Competences</label>
                                                <select class="form-control" name="dtpengembangan[competences_png][]"
                                                    id="competences_png"
                                                    ng-model="assessreport.competences_pengembangan"
                                                    ng-change="setBhp()">
                                                    <option value="" selected>-- Choose Competences --</option>
                                                    <option ng-repeat="(kcom, vcom) in list_competence"
                                                        ng-bind="vcom.competence_name" value="{{vcom.competence_name}}">
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="behavior_png">Behavior <span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control"
                                                    name="dtpengembangan-{{assessreport.competences_pengembangan}}[behavior_png][]"
                                                    id="behavior_png" ng-model="assessreport.behavior_name"
                                                    ng-change="setPgm()">
                                                    <option value="" selected>-- Choose behavior --</option>
                                                    <option ng-repeat="(kvior, vior) in list_behavior_pengembangan"
                                                        ng-bind="vior.behavior_name" value="{{vior.behavior_name}}">
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group"
                                                ng-repeat="(a, b) in assessreport.mandiri track by $index">
                                                <label for="mandiri">Mandiri <span class="text-danger">*</span></label>
                                                <div class="row">
                                                    <div class="col-10 col-md-10 col-lg-10">
                                                        <select class="form-control"
                                                            name="dtpengembangan-{{assessreport.competences_pengembangan}}[mandiri][]"
                                                            id="mandiri" ng-model="assessreport.mandiri[a]">
                                                            <option value="" selected>-- Choose Mandiri --</option>
                                                            <option ng-repeat="(kmand, vmand) in list_mandiri"
                                                                ng-bind="vmand" value="{{vmand}}"></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-2 col-md-2 col-lg-2">
                                                        <button type="button" class="btn btn-success btn-add-crqnumber"
                                                            ng-click="multiMandiri('add',a)"><i
                                                                class="fas fa-plus"></i></button>
                                                        <button type="button" class="btn btn-danger btn-del-crqnumber"
                                                            ng-click="multiMandiri('rm',a)"><i
                                                                class="fas fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"
                                                ng-repeat="(a, b) in assessreport.penugasan track by $index">
                                                <label for="penugasan">Penugasan <span
                                                        class="text-danger">*</span></label>
                                                <div class="row">
                                                    <div class="col-10 col-md-10 col-lg-10">
                                                        <select class="form-control"
                                                            name="dtpengembangan-{{assessreport.competences_pengembangan}}[penugasan][]"
                                                            id="penugasan" ng-model="assessreport.penugasan[a]">
                                                            <option value="" selected>-- Choose penugasan --</option>
                                                            <option ng-repeat="(kpen, vpen) in list_penugasan"
                                                                ng-bind="vpen" value="{{vpen}}"></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-2 col-md-2 col-lg-2">
                                                        <button type="button" class="btn btn-success btn-add-crqnumber"
                                                            ng-click="multiPenugasan('add',a)"><i
                                                                class="fas fa-plus"></i></button>
                                                        <button type="button" class="btn btn-danger btn-del-crqnumber"
                                                            ng-click="multiPenugasan('rm',a)"><i
                                                                class="fas fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"
                                                ng-repeat="(a, b) in assessreport.training_seminar track by $index">
                                                <label for="training_seminar">Training / Seminar <span
                                                        class="text-danger">*</span></label>
                                                <div class="row">
                                                    <div class="col-10 col-md-10 col-lg-10">
                                                        <select class="form-control"
                                                            name="dtpengembangan[training_seminar][]"
                                                            id="training_seminar"
                                                            ng-model="assessreport.training_seminar[a]">
                                                            <option value="" selected>-- Choose training_seminar --
                                                            </option>
                                                            <option ng-repeat="(kpen, vpen) in list_training_seminar"
                                                                ng-bind="vpen" value="{{vpen}}"></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-2 col-md-2 col-lg-2">
                                                        <button type="button" class="btn btn-success btn-add-crqnumber"
                                                            ng-click="multiTraining_seminar('add',a)"><i
                                                                class="fas fa-plus"></i></button>
                                                        <button type="button" class="btn btn-danger btn-del-crqnumber"
                                                            ng-click="multiTraining_seminar('rm',a)"><i
                                                                class="fas fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary float-right" type="submit"><i
                                            class="fas fa-check-circle"></i> Generate</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
</form>
</section>
</div>