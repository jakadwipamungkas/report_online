<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Competence_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("competence.*, clusters.cluster_name")
                        ->from("competence")
                        ->join("clusters", "clusters.id = competence.cluster_id")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function cluster_get()
    {
        $sql = $this->db->select("id, cluster_name")
                        ->from("clusters")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();

        $data = [
            "competence_name"  => $formData["competence_name"],
            "competence_desc"  => $formData["competence_desc"],
            "cluster_id"     => $formData["cluster_id"],
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Competence_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "competence_name"  => $formData["competence_name"],
            "competence_desc"  => $formData["competence_desc"],
            "cluster_id"       => $formData["cluster_id"],
            "updated_by"       => $this->session->userdata("users")["fullname"],
            "updated_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Competence_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Competence_model->delete($formData["id"]);

        $this->response($sql);
    }
}
