<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Competence extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Competence_model"
        ]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Competences';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/competence.js?' . rand()
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/competence.css?'. rand()
		);

		$this->data['list_competence'] 	= $this->Competence_model->get();
		$this->data['list_cluster'] 	= $this->Competence_model->getCluster();

		insertLog("Competences", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

	public function process_post()
	{
		$formData = $this->post();

        $data = [
            "competence_name"  => $formData["competence_name"],
            "competence_desc"  => $formData["competence_desc"],
            "cluster_id"       => $formData["cluster_id"],
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Competence_model->save($data);
		redirect("competence");
	}
}
