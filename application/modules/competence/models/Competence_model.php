<?php
class Competence_model extends CI_Model {

    public function get()
    {
        $sql = $this->db->select("competence.*, clusters.cluster_name")
                        ->from("competence")
                        ->join("clusters", "clusters.id = competence.cluster_id")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function getCluster()
    {
        $sql = $this->db->select("id, cluster_name")
                        ->from("clusters")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function save($data)
    {

        $query = $this->db->insert("competence", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully created, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function update($id, $data)
    {

        $query = $this->db->where("id", $id)->update("competence", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully updated, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function delete($id)
    {

        $query = $this->db->where("id", $id)->delete("competence");

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully deleted, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

}