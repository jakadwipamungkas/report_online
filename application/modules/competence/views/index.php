<div class="main-content" ng-controller="competence">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Data Competence</h4>
                  <div class="card-header-action">
                    <a href="" ng-click="clickAdd()" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Competence Name</th>
                                <th>Competence Description</th>
                                <th>Cluster Name</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <!-- <tr ng-repeat="(key, val) in list_competence">
                                <td ng-bind="(key+1)"></td>
                                <td ng-bind="val.competence_name"></td>
                                <td ng-bind="val.competence_desc"></td>
                                <td ng-bind="val.cluster_name"></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit(val)" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.competence_name)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr> -->
                              <?php foreach ($list_competence["data"] as $key => $value) { ?>
                              <tr>
                                <td><?= ($key+1) ?></td>
                                <td><?= $value["competence_name"] ?></td>
                                <td><?= $value["competence_desc"] ?></td>
                                <td><?= $value["cluster_name"] ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit('<?= base64_encode(json_encode($value)) ?>')" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete('<?= $value["id"] ?>','<?= $value["competence_name"] ?>')" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>

    <!-- Modal Add -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAddLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAddLabel">Add competence</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?= base_url("competence/process") ?>" method="POST">
                <div class="form-group">
                  <label for="competence_name">Competences Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="competence_name" name="competence_name" required>
                </div>
                <div class="form-group">
                  <label for="competence_desc">Competences Description <span class="text-danger">*</span></label>
                  <textarea class="form-control" id="competence_desc" name="competence_desc" style="height: 130px;" required></textarea>
                </div>
                <div class="form-group">
                  <label for="cluster_name">Cluster Name <span class="text-danger">*</span></label>
                  <select class="form-control" name="cluster_id" id="cluster_name" required>
                    <option value="" selected>-- Choose Cluster --</option>
                    <?php foreach ($list_cluster["data"] as $key => $value) { ?>
                      <option value="<?= $value["id"] ?>"><?= $value["cluster_name"] ?></option>
                    <?php } ?>
                    <!-- <option ng-repeat="(kc, vc) in list_cluster" ng-bind="vc.cluster_name" value="{{vc.id}}"></option> -->
                  </select>
                </div>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="submit" class="btn btn-primary btn-block m-0 btn-save" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="mdlEdit" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlEditLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlEditLabel">Add competence</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="competence_name_edit">competence Name <span class="text-danger">*</span></label>
                  <input type="hidden" class="form-control" id="competence_name_id" ng-model="editcompetence.id">
                  <input type="text" class="form-control" id="competence_name_edit" ng-model="editcompetence.competence_name">
                </div>
                <div class="form-group">
                  <label for="competence_desc">Competences Description <span class="text-danger">*</span></label>
                  <textarea class="form-control" id="competence_desc" ng-model="editcompetence.competence_desc" style="height: 130px;"></textarea>
                </div>
                <div class="form-group">
                  <label for="cluster_name">Cluster Name <span class="text-danger">*</span></label>
                  <select class="form-control" name="cluster_name" id="cluster_name" ng-model="editcompetence.cluster_id">
                    <option value="" selected>-- Choose Cluster --</option>
                    <option ng-repeat="(kc, vc) in list_cluster" ng-bind="vc.cluster_name" value="{{vc.id}}"></option>
                  </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0 btn-update" ng-click="update()"><i class="fas fa-check-circle"></i> Update</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" tabindex="-1" role="dialog" aria-labelledby="mdlDelete" aria-hidden="true">
      <div class="modal-dialog modal-sm modal-dialog-centered">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">
              <i class="fas fa-exclamation-circle text-warning mt-3 mb-5" style="font-size: 40px"></i><br>
              <h6>Are you sure want to delete this competence <b class="text-danger" ng-bind="deletecompetence.competence_name"></b> ?</h6>
              <form>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="competence_name_delete_id" ng-model="deletecompetence.id">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="button" class="btn btn-primary btn-block m-0 btn-delete" ng-click="delete()"><i class="fas fa-check-circle"></i> Delete</button>
              </div>
            </div>
          </div>
      </div>
    </div> 
</div>