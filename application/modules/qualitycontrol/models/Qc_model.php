<?php
class Qc_model extends CI_Model {

    public function save($data)
    {

        $query = $this->db->insert("quality_control", $data);

        if ($query) {
            $updateParticipant = $this->db->where("id", $data["participant_id"])->update("participant", ["qc_id" => $data["qc_uid"]]);
            $return = [
                "error"     => false,
                "message"   => "Data successfully created, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function update($id, $data)
    {

        $query = $this->db->where("id", $id)->update("quality_control", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully updated, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function delete($id)
    {

        $query = $this->db->where("id", $id)->delete("quality_control");

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully deleted, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

}