<div class="main-content" ng-controller="qualitycontrol">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Data Quality Control</h4>
                  <div class="card-header-action">
                    <a href="" ng-click="clickAdd()" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="#" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Participant Name</th>
                                <th>Assessor Name</th>
                                <th>Quality Control Name</th>
                                <th>Quality Control Degree</th>
                                <th>Quality Control SIPP</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="(key, val) in list_qualitycontrol">
                                <td ng-bind="(key+1)"></td>
                                <td class="text-left" ng-bind="val.participant_name"></td>
                                <td ng-bind="val.assess_name"></td>
                                <td ng-bind="val.qc_name"></td>
                                <td ng-bind="val.qc_degree"></td>
                                <td ng-bind="val.qc_sipp"></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" class="btn btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAddLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAddLabel">Add Quality Control</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="assessor_name">Assessor Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="assessor_name" ng-model="add.assessor_id" ng-change="setData()">
                    <option value="" selected>Assessor Name</option>
                    <option ng-repeat="(kus, vus) in list_assessor" ng-bind="vus.fullname" value="{{vus.user_id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="name_participant">Participant Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="name_participant" ng-model="add.participant_name">
                    <option value="" selected>-- Choose Participant Name --</option>
                    <option ng-repeat="(kds, vds) in dataassess" ng-bind="vds.participant_name" value="{{vds.participant_id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="quality_control_name">Quality Control Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="qc_uid" ng-model="add.qc_uid" ng-change="others()">
                    <option value="" selected>-- Choose Quality Control --</option>
                    <option ng-repeat="(kus, vus) in list_users" ng-bind="vus.fullname" value="{{vus.id}}"></option>
                    <option value="others">Add QC</option>
                  </select>
                </div>
                <div class="form-group frm_others d-none">
                  <label for="usr_qualityname">Quality Control Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="usr_qualityname" ng-model="add.usr_qualityname">
                </div>
                <div class="form-group frm_others d-none">
                  <label for="username">Username <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="username" ng-model="add.username">
                </div>
                <div class="form-group frm_others d-none">
                  <label for="email">Email <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="email" ng-model="add.email">
                </div>
                <div class="form-group frm_others d-none">
                  <label for="password">Password <span class="text-danger">*</span></label>
                  <input type="password" class="form-control" id="password" ng-model="add.password" readonly>
                </div>
                <div class="form-group">
                  <label for="quality_control_degree">Quality Control Degree <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="quality_control_degree" ng-model="add.degree">
                </div>
                <div class="form-group">
                  <label for="quality_control_sipp">Quality Control SIPP <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="quality_control_sipp" ng-model="add.sipp">
                </div>
                <div class="form-group">
                  <label for="assessor_sipp">Status Assessor<span class="text-danger">*</span></label>
                  <select class="custom-select" ng-model="add.status">
                    <option selected>-- Choose Status --</option>
                    <option value="active">Active</option>
                    <option value="non-active">Non Active</option>
                  </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAddLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAddLabel">Add Quality Control</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="assessor_name">Assessor Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="assessor_name" ng-model="editqc.assessor_id" ng-change="setData()">
                    <option value="" selected>Assessor Name</option>
                    <option ng-repeat="(kus, vus) in list_assessor" ng-bind="vus.fullname" value="{{vus.user_id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="participant_name">Participant Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="participant_name" ng-model="editqc.participant_name">
                    <option value="" selected>-- Choose Participant Name --</option>
                    <option ng-repeat="(kds, vds) in dataassess" ng-bind="vds.participant_name" value="{{vds.participant_name}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="quality_control_name">Quality Control Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="qc_uid" ng-model="editqc.qc_uid">
                    <option value="" selected>-- Choose Quality Control --</option>
                    <option ng-repeat="(kus, vus) in list_users" ng-bind="vus.fullname" value="{{vus.id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="quality_control_degree">Quality Control Degree <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="quality_control_degree" ng-model="editqc.degree">
                </div>
                <div class="form-group">
                  <label for="quality_control_sipp">Quality Control SIPP <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="quality_control_sipp" ng-model="editqc.sipp">
                </div>
                <div class="form-group">
                  <label for="assessor_sipp">Status Assessor<span class="text-danger">*</span></label>
                  <select class="custom-select" ng-model="add.status">
                    <option selected>-- Choose Status --</option>
                    <option value="active">Active</option>
                    <option value="non-active">Non Active</option>
                  </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>