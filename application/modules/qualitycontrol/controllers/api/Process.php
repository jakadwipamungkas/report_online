<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Qc_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("quality_control.*, assessor.user_id as assess_uid, uq.fullname as qc_name, ua.fullname as assess_name, participant.participant_name")
                        ->from("quality_control")
                        ->join("assessor", "assessor.id = quality_control.assessor_id", "left")
                        ->join("users uq", "uq.id = quality_control.qc_uid", "left")
                        ->join("users ua", "ua.id = quality_control.assessor_id", "left")
                        ->join("participant", "participant.id = quality_control.participant_id", "left")
                        ->get()
                        
                        ->result_array();
                        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function detail_assess_get()
    {
        $sql = $this->db->select("assessor.*, users.fullname, participant.participant_name")
                        ->from("assessor")
                        ->join("users", "users.id = assessor.user_id")
                        ->join("participant", "participant.id = assessor.participant_id")
                        ->where("assessor.user_id", $this->get("assessor_id"))
                        ->where("participant.qc_id IS NULL")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function users_data_get()
    {
        $sql = $this->db->select("id, fullname, username")
                        ->from("users")
                        ->where("role_id", 4)
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function assessor_get()
    {
        $sql = $this->db->select("assessor.*, users.fullname")
                        ->from("assessor")
                        ->join("users", "users.id = assessor.user_id")
                        ->group_by("assessor.user_id")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();

        // print_r("<pre>");
        // print_r($formData);
        // exit();
        if ($formData["qc_uid"] == "others") {

            $dt_user = [
                "fullname"  => $formData["usr_qualityname"],
                "username"  => $formData["username"],
                "email"     => $formData["email"],
                "role_id"   => 4,
                "password"  => setEncrypt($formData["password"]),
                "temp_pass" => $formData["password"],
                "status"    => "active",
                "created_by"    => $this->session->userdata("users")["fullname"],
                "created_at"    => date("Y-m-d H:i:s")
            ];

            $crt_usr = $this->db->insert("users", $dt_user);
            $insert_id = $this->db->insert_id();

            $data = [
                "qc_uid"            => $insert_id,
                "assessor_id"       => $formData["assessor_id"],
                "participant_id"    => $formData["participant_name"],
                // "assessor_id"       => $formData["assessor_id"],
                "qc_degree"         => $formData["degree"],
                "qc_sipp"           => $formData["sipp"],
                "status_qc"         => $formData["status"],
                "status_report"     => "waiting",
                "created_by"        => $this->session->userdata("users")["fullname"],
                "created_at"        => date("Y-m-d H:i:s")
            ];
            // print_r("<pre>");
            // print_r($data);
            // exit();
    
            $sql = $this->Qc_model->save($data);
    
            $this->response($sql);

        } else {

            $data = [
                "qc_uid"            => $formData["qc_uid"],
                "assessor_id"       => $formData["assessor_id"],
                "participant_id"    => $formData["participant_name"],
                // "assessor_id"       => $formData["assessor_id"],
                "qc_degree"         => $formData["degree"],
                "qc_sipp"           => $formData["sipp"],
                "status_qc"         => $formData["status"],
                "status_report"     => "waiting",
                "created_by"        => $this->session->userdata("users")["fullname"],
                "created_at"        => date("Y-m-d H:i:s")
            ];
            // print_r("<pre>");
            // print_r($data);
            // exit();
    
            $sql = $this->Qc_model->save($data);
    
            $this->response($sql);

        }
    }
}
