<div class="main-content" ng-controller="users_list">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>User Role</h4>
                  <div class="card-header-action">
                    <a href="#" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="#" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="tbl-role">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Role Name</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($list_role as $key => $value) { ?>
                                <tr>
                                  <td><?= ($key+1) ?></td>
                                  <td><?= $value["role_name"] ?></td>
                                  <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit('<?= base64_encode(json_encode($value)) ?>')" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.fullname)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                    </div>
                                  </td>
                                </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
</div>