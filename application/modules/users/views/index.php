<div class="main-content" ng-controller="users_list">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>User List</h4>
                  <div class="card-header-action">
                    <a href="" ng-click="clickAdd()" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="" ng-click="export()" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table datatable="ng" class="table table-striped">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Full Name</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>User Role</th>
                                <th>Status User</th>
                                <th>Last Login</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($list_user as $key => $value) { ?>
                                <tr>
                                  <td><?= ($key+1) ?></td>
                                  <td><?= $value["fullname"] ?></td>
                                  <td><?= $value["username"] ?></td>
                                  <td><?= $value["email"] ?></td>
                                  <td><?= $value["role_name"] ?></td>
                                  <td>
                                    <?php if($value["status"] == "active") { ?>
                                      <span class="badge badge-success"><?= $value["status"] ?></span>
                                    <?php } else if($value["status"] == "nonactive") { ?>
                                        <span class="badge badge-success"><?= $value["status"] ?></span>
                                    <?php } ?>
                                  </td>
                                  <td><?= $value["last_login"] ?></td>
                                  <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit('<?= base64_encode(json_encode($value)) ?>')" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.fullname)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                    </div>
                                  </td>
                                </tr>
                              <?php } ?>
                              <!-- <tr ng-repeat="(key, val) in list_users">
                                <td ng-bind="(key+1)"></td>
                                <td class="text-left" ng-bind="val.fullname"></td>
                                <td ng-bind="val.username"></td>
                                <td ng-bind="val.email"></td>
                                <td ng-if="val.role_id == 1">Admin</td>
                                <td ng-if="val.role_id == 2">Assessor</td>
                                <td ng-if="val.role_id == 3">Quality Control</td>
                                <td ng-if="val.status == 'active'">
                                  <span class="badge badge-success" ng-bind="val.status"></span>
                                </td>
                                <td ng-if="val.status == 'nonactive'">
                                  <span class="badge badge-danger" ng-bind="val.status"></span>
                                </td>
                                <td ng-bind="val.last_login"></td>
                                <td>
                                  <div class="btn-group">
                                        <a href="" ng-click="clickEdit(val)" class="btn btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.fullname)" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr> -->
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
    <!-- Modal Add -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAddLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAddLabel">Add Users</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="<?= base_url("users/process") ?>" method="POST">
            <div class="modal-body">
              <div class="form-group">
                <label for="full_name">Full Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="full_name" name="full_name" required>
              </div>
              <div class="form-group">
                <label for="users_name">User Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="users_name" name="users_name" required>
              </div>
              <div class="form-group">
                <label for="email">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control" id="email" name="email" required>
              </div>
              <div class="form-group">
                <label for="password">Password <span class="text-danger">*</span></label>
                <input type="password" class="form-control" id="password" name="password" required>
              </div>
              <div class="form-group">
                <label for="role">Role Akses<span class="text-danger">*</span></label>
                <select class="form-control" id="role" name="role" required>
                  <option value="" selected>-- Choose Role --</option>
                  <option value="1">Developer</option>
                  <option value="2">Admin</option>
                  <option value="3">Assessor</option>
                  <option value="4">Quality Control</option>
                </select>
              </div>
              <div class="form-group">
                <label for="status">Status<span class="text-danger">*</span></label>
                <select class="form-control" id="status" name="status" required>
                  <option value="" selected>-- Choose Status --</option>
                  <option value="active">Active</option>
                  <option value="nonactive">Non Active</option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="submit" class="btn btn-primary btn-block m-0 btn-save"><i class="fas fa-check-circle"></i> Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="mdlEdit" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlEditLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlEditLabel">Add Cluster</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="">
            <div class="modal-body">
                <div class="form-group">
                  <label for="full_name">Full Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="full_name_edit" name="full_name_edit" value="{{editusers.fullname}}" required>
                </div>
                <div class="form-group">
                  <label for="users_name">User Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="users_name_edit" name="users_name_edit" value="{{editusers.username}}" required>
                </div>
                <div class="form-group">
                  <label for="email">Email <span class="text-danger">*</span></label>
                  <input type="email" class="form-control" id="email_edit" name="email_edit" value="{{editusers.email}}" required>
                </div>
                <div class="form-group">
                  <label for="password">Password <span class="text-danger">*</span></label>
                  <input type="hidden" class="form-control" id="password_status" name="password_status" value="{{editusers.temp_pass}}" required>
                  <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                      <input type="password" class="form-control pass_edit" id="password_edit" name="password_edit" required>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 p-1">
                      <button type="button" class="btn btn-info btn-change-pass" ng-click="changePass()"><i class="fas fa-pen"></i> Change Password</button>
                      <button type="button" class="btn btn-danger btn-cancel-pass d-none" ng-click="cancelPass()"><i class="fas fa-times-circle"></i> Cancel</button>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="role">Role Akses<span class="text-danger">*</span></label>
                  <select class="form-control" id="role_edit" name="role_edit" value="{{editusers.role_id}}" ng-model="editusers.role_id" required>
                    <option value="" selected>-- Choose Role --</option>
                    <option value="1">Developer</option>
                    <option value="2">Admin</option>
                    <option value="3">Assessor</option>
                    <option value="4">Quality Control</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="status">Status<span class="text-danger">*</span></label>
                  <select class="form-control" id="status_edit" name="status_edit" value="{{editusers.status}}" ng-model="editusers.status" required>
                    <option value="" selected>-- Choose Status --</option>
                    <option value="active">Active</option>
                    <option value="nonactive">Non Active</option>
                  </select>
                </div>
              </div>
          </form>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="submit" class="btn btn-primary btn-block m-0 btn-update"><i class="fas fa-check-circle"></i> Update</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" tabindex="-1" role="dialog" aria-labelledby="mdlDelete" aria-hidden="true">
      <div class="modal-dialog modal-sm modal-dialog-centered">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">
              <i class="fas fa-exclamation-circle text-warning mt-3 mb-5" style="font-size: 40px"></i><br>
              <h6>Are you sure want to delete this User <b class="text-danger" ng-bind="deleteusers.users_name"></b> ?</h6>
              <form>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="users_name_delete_id" required>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="button" class="btn btn-primary btn-block m-0 btn-delete" ng-click="delete()"><i class="fas fa-check-circle"></i> Delete</button>
              </div>
            </div>
          </div>
      </div>
    </div> 
</div>