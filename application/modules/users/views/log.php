<div class="main-content" ng-controller="users_list">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Users Log</h4>
                  <div class="card-header-action">
                    <!-- <a href="#" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="#" class="btn btn-info"><i class="fas fa-download"></i> Export</a> -->
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>IP Address</th>
                                <th>Full Name</th>
                                <th>Browser</th>
                                <th>Modul</th>
                                <th>Page</th>
                                <th>Access Time</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="(klog, vlog) in list_log">
                                  <td class="text-left" ng-bind="(klog+1)"></td>
                                  <td class="text-left" ng-bind="vlog.ip"></td>
                                  <td class="text-left" ng-bind="vlog.fullname"></td>
                                  <td class="text-left" ng-bind="vlog.browser"></td>
                                  <td class="text-left" ng-bind="vlog.modul"></td>
                                  <td class="text-left" ng-bind="vlog.vmain"></td>
                                  <td class="text-left" ng-bind="vlog.access_time"></td>
                              </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
</div>