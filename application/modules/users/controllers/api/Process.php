<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Users_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->Users_model->get();

        $this->response($sql);

	}

    public function index_post()
    {
        $formData = $this->input->post();

        $data = [
            "fullname"  => $formData["fullname"],
            "username"  => $formData["username"],
            "email"     => $formData["email"],
            "role_id"   => $formData["role_id"],
            "password"  => setEncrypt($formData["password"]),
            "temp_pass" => $formData["password"],
            "status"    => "active",
            "created_by"    => $this->session->userdata("users")["fullname"],
            "created_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Users_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $checkuser = $this->db->select("temp_pass")
                                ->from("users")
                                ->where("id", $this->post("id"))
                                ->get()
                                ->first_row();

        $data = [
            "fullname"  => $formData["fullname"],
            "username"  => $formData["username"],
            "email"     => $formData["email"],
            "password"  => $formData["status_password"] == "change" ? setEncrypt($formData["password"]) : setEncrypt($checkuser->temp_pass),
            "temp_pass" => $formData["status_password"] == "change" ? ($formData["password"]) : ($checkuser->temp_pass),
            "status"    => "active",
            "updated_by"    => $this->session->userdata("users")["fullname"],
            "updated_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Users_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Users_model->delete($formData["id"]);

        $this->response($sql);
    }

    public function logdata_get()
	{
        $sql = $this->db->select("*")
                        ->from("users_log")
                        ->order_by("access_time", "DESC")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}
}
