<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Users extends Iacr_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Users_model"
        ]);
	}

	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Users';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			'assets/app/js/users.js'
		);

		$this->data['css'] = array(
			'assets/app/css/users.css'
		);

		$getUser = $this->Users_model->get()["data"];
		
		$this->data['list_user']	= $getUser;
		
		insertLog("Users List", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

	public function process_post()
    {
        $formData = $this->post();

        $data = [
            "fullname"  => $formData["full_name"],
            "username"  => $formData["users_name"],
            "email"     => $formData["email"],
            "role_id"   => $formData["role"],
            "password"  => setEncrypt($formData["password"]),
            "temp_pass" => $formData["password"],
            "status"    => "active",
            "created_by"    => $this->session->userdata("users")["fullname"],
            "created_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Users_model->save($data);
		redirect("users");
    }

	public function Role_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Users Role';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			'assets/app/js/users.js'
		);

		$this->data['css'] = array(
			'assets/app/css/users.css'
		);

		$sql = $this->Users_model->getRole();
		
		$this->data["list_role"] = $sql["data"];

		insertLog("Role List", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'role');

	}

	public function Log_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Users Log';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			'assets/app/js/users.js'
		);

		$this->data['css'] = array(
			'assets/app/css/users.css'
		);

		insertLog("Users Log", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'log');

	}
}
