<?php
class Users_model extends CI_Model {

    public function get()
    {
        $query = $this->db->select("users.*, users_role.role_name")
                            ->from("users")
                            ->join("users_role", "users.role_id = users_role.id", "left")
                            ->get()
                            ->result_array();
        
        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data found",
                "data"      => $query
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Data empty",
                "data"      => []
            ];
        }

        return $return;
    }
    
    public function save($data)
    {

        $query = $this->db->insert("users", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully created, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function update($id, $data)
    {

        $query = $this->db->where("id", $id)->update("users", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully updated, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function delete($id)
    {

        $query = $this->db->where("id", $id)->delete("users");

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully deleted, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function getRole()
    {
        $query = $this->db->select("users_role.*")
                            ->from("users_role")
                            ->get()
                            ->result_array();
        
        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data found",
                "data"      => $query
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Data empty",
                "data"      => []
            ];
        }

        return $return;
    }

}