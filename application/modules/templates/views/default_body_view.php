<body ng-app="acr">
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <form class="form-inline mr-auto">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                        <li class="text-white d-none d-sm-block"><?= date("j F Y") ?> <span id="jam"></span></li>
                    </ul>
                    
                    <div class="search-element">
                        <div class="text-white">
                            <!-- <?= date("j F Y") ?> <span id="jam"></span> -->
                        </div>
                    </div>
                </form>
                <ul class="navbar-nav navbar-right">
                    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link nav-link-lg message-toggle beep"><i class="far fa-envelope"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right">
                        <div class="dropdown-header">Messages
                            <div class="float-right">
                            <a href="#">Mark All As Read</a>
                            </div>
                        </div>
                        <div class="dropdown-list-content dropdown-list-message">
                            <a href="#" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-avatar">
                                <img alt="image" src="<?= base_url() ?>assets/web/img/avatar/avatar-1.png" class="rounded-circle">
                                <div class="is-online"></div>
                            </div>
                            <div class="dropdown-item-desc">
                                <b>Kusnaedi</b>
                                <p>Hello, Bro!</p>
                                <div class="time">10 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-avatar">
                                <img alt="image" src="<?= base_url() ?>assets/web/img/avatar/avatar-2.png" class="rounded-circle">
                            </div>
                            <div class="dropdown-item-desc">
                                <b>Dedik Sugiharto</b>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                                <div class="time">12 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-avatar">
                                <img alt="image" src="<?= base_url() ?>assets/web/img/avatar/avatar-3.png" class="rounded-circle">
                                <div class="is-online"></div>
                            </div>
                            <div class="dropdown-item-desc">
                                <b>Agung Ardiansyah</b>
                                <p>Sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <div class="time">12 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item">
                            <div class="dropdown-item-avatar">
                                <img alt="image" src="<?= base_url() ?>assets/web/img/avatar/avatar-4.png" class="rounded-circle">
                            </div>
                            <div class="dropdown-item-desc">
                                <b>Ardian Rahardiansyah</b>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit ess</p>
                                <div class="time">16 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item">
                            <div class="dropdown-item-avatar">
                                <img alt="image" src="<?= base_url() ?>assets/web/img/avatar/avatar-5.png" class="rounded-circle">
                            </div>
                            <div class="dropdown-item-desc">
                                <b>Alfa Zulkarnain</b>
                                <p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
                                <div class="time">Yesterday</div>
                            </div>
                            </a>
                        </div>
                        <div class="dropdown-footer text-center">
                            <a href="#">View All <i class="fas fa-chevron-right"></i></a>
                        </div>
                        </div>
                    </li>
                    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right">
                        <div class="dropdown-header">Notifications
                            <div class="float-right">
                            <a href="#">Mark All As Read</a>
                            </div>
                        </div>
                        <div class="dropdown-list-content dropdown-list-icons">
                            <a href="#" class="dropdown-item dropdown-item-unread">
                            <div class="dropdown-item-icon bg-primary text-white">
                                <i class="fas fa-code"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Template update is available now!
                                <div class="time text-primary">2 Min Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item">
                            <div class="dropdown-item-icon bg-info text-white">
                                <i class="far fa-user"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                <b>You</b> and <b>Dedik Sugiharto</b> are now friends
                                <div class="time">10 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item">
                            <div class="dropdown-item-icon bg-success text-white">
                                <i class="fas fa-check"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                                <div class="time">12 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item">
                            <div class="dropdown-item-icon bg-danger text-white">
                                <i class="fas fa-exclamation-triangle"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Low disk space. Let's clean it!
                                <div class="time">17 Hours Ago</div>
                            </div>
                            </a>
                            <a href="#" class="dropdown-item">
                            <div class="dropdown-item-icon bg-info text-white">
                                <i class="fas fa-bell"></i>
                            </div>
                            <div class="dropdown-item-desc">
                                Welcome to Assessment Center
                                <div class="time">Yesterday</div>
                            </div>
                            </a>
                        </div>
                        <div class="dropdown-footer text-center">
                            <a href="#">View All <i class="fas fa-chevron-right"></i></a>
                        </div>
                        </div>
                    </li>
                    <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <img alt="image" src="<?= base_url() ?>assets/web/img/avatar/avatar-1.png" class="rounded-circle mr-1">
                        <div class="d-sm-none d-lg-inline-block">Hi, <?= $users["fullname"] ?></div></a>
                        <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-title">Masuk sebagai:
                            <?php if ($users["role_id"] == 1) { ?>
                                <b>Developer</b>
                            <?php } elseif ($users["role_id"] == 2) { ?>
                                <b>Admin</b>
                            <?php } elseif ($users["role_id"] == 3) { ?>
                                <b>Assessor</b>
                            <?php } elseif ($users["role_id"] == 4) { ?>
                                <b>Quality Control</b>
                            <?php } ?>
                        </div>
                        <a href="features-profile.html" class="dropdown-item has-icon">
                            <i class="far fa-user"></i> Profil
                        </a>
                        <a href="features-activities.html" class="dropdown-item has-icon">
                            <i class="fas fa-bolt"></i> Log Aktifitas
                        </a>
                        <a href="features-settings.html" class="dropdown-item has-icon">
                            <i class="fas fa-cog"></i> Pengaturan
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="<?= base_url("logout") ?>" class="dropdown-item has-icon text-danger">
                            <i class="fas fa-sign-out-alt"></i> Keluar
                        </a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="main-sidebar">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="index.html">
                            <img src="<?= base_url() ?>assets/web/img/ptc-icon.png" class="mr-2" style="width: 12%;">Assessment Report</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="index.html"><img src="<?= base_url() ?>assets/web/img/ptc-icon.png" style="width: 42%;"></a>
                    </div>
                        <ul class="sidebar-menu">
                            <li class="menu-header">Monitoring</li>
                            <li class="nav-item">
                                <a href="<?= base_url("dashboard") ?>" class="nav-link"><i class="fas fa-stream"></i><span>Monitoring Assessment</span></a>
                            </li>
                            
                            <?php if ($users["role_id"] == 1) { ?>
                                <li class="menu-header">Participant</li>
                                <li class="nav-item">
                                    <a href="<?= base_url("participant") ?>" class="nav-link"><i class="fas fa-users"></i><span>Participant List</span></a>
                                </li>
                                <li class="menu-header">Master Data</li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("assessor") ?>"><i class="fas fa-user-tie"></i> <span>Assessor</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("qualitycontrol") ?>"><i class="fas fa-chalkboard-teacher"></i> <span>Quality Control</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("cluster") ?>"><i class="far fa-file-alt"></i> <span>Clusters</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("competence") ?>"><i class="far fa-file-alt"></i> <span>Competences</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("behavior") ?>"><i class="far fa-file-alt"></i> <span>Behaviors</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("glossary") ?>"><i class="far fa-file-alt"></i> <span>Glossary Statements</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url("pengembangan") ?>"><i class="far fa-file-alt"></i> <span>Saran Pengembangan</span></a>
                                </li>
                            <?php } ?>

                        </ul>
                        <?php if ($users["role_id"] == 1) { ?>
                            <ul class="sidebar-menu">
                                <li class="menu-header">Users Management</li>
                                <li><a class="nav-link" href="<?= base_url("users") ?>"><i class="fas fa-users-cog"></i> <span>Users List</span></a></li>
                                <li><a class="nav-link" href="<?= base_url("users/role") ?>"><i class="fas fa-user-tag"></i> <span>Users Role</span></a></li>
                                <li><a class="nav-link" href="<?= base_url("users/log") ?>"><i class="fas fa-list"></i> <span>Users Log</span></a></li>
                            </ul>
                        <?php } ?>
                    
                    <?php if ($users["role_id"] == 3) { ?>
                        <ul class="sidebar-menu">
                            <li><a class="nav-link" href="<?= base_url("assessment") ?>"><i class="fas fa-users-cog"></i> <span>Assessment</span></a></li>
                        </ul>
                    <?php } ?>

                    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="<?= base_url("logout") ?>" class="btn btn-primary btn-lg btn-block btn-icon-split">
                            <i class="fas fa-rocket"></i> Keluar
                        </a>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    <?= $body ?>
    <div class="modal fade" id="popup-msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
      <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">

            </div>
            <div class="loader">
              <button class="btn-danger" style="display: none;"></button>
              <button class="btn-success" style="display: none;"></button>
            </div>
          </div>
      </div>
    </div> 
</body>