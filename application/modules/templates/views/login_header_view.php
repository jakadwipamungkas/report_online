<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from brandio.io/envato/iofrm/html/login9.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 Jul 2020 08:00:31 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/iofrm-theme9.css">
    <link rel="stylesheet" href="<?= base_url('assets/login/css/azzara.min.css') ?>">

    <link rel="icon" href="<?= base_url() ?>assets/web/img/ptc-icon.png" type="image/x-icon">
    
    <?php
            if (isset($css) && count($css) > 0) {
                foreach ($css as $key => $vcss) {
                    echo '<link rel="stylesheet" href="' . (is_int($key) ? base_url($vcss) : $vcss) . '">';
                }
            }
        ?>
</head>