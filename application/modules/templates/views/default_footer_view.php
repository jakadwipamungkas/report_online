    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?= base_url() ?>assets/web/js/stisla.js"></script>

    <!-- Template JS File -->
    <script src="<?= base_url() ?>assets/web/js/scripts.js"></script>
    <script src="<?= base_url() ?>assets/web/js/custom.js"></script>

    <!-- Page Specific JS File -->
    <script src="<?= base_url() ?>assets/web/js/page/index-0.js"></script>

    <!-- AngularJS -->
    <script type="text/javascript" src="<?= base_url('resources/plugins/angular/1.7.9/angular.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('resources/plugins/angular/1.7.9/angular-cookies.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('resources/plugins/angular/1.7.9/angular-sanitize.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('resources/plugins/angular/dirPagination.js') ?>"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.4/angular-datatables.min.js"></script>
    <!-- end angularjs -->

    <!-- Select 2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- end Select 2 -->

    <script type="text/javascript" src="<?= base_url('assets/app.js?') . rand() ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/main.js') ?>"></script>

    <?php
        if (isset($js) && count($js) > 0) {
            foreach ($js as $key => $vjs) {
                echo '<script type="text/javascript" src="' . (is_int($key) ? base_url($vjs) : $vjs) . '"></script>';
            }
        }
    ?>
    <script>
        $(function(){
            var current = location.pathname;
            console.log(window.location.pathname);
            $('.sidebar-menu .nav-item a').each(function(){
                var $this = $(this);

                if($this.attr('href').indexOf(current) !== -1){
                    $this.addClass('active-link');
                }
            })
        });

        function startTime() {
            var today=new Date();
            var h=today.getHours();
            var m=today.getMinutes();
            var s=today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('jam').innerHTML = h+":"+m+":"+s;
            var t = setTimeout(function(){startTime()},500);
        }

        function checkTime(i) {
            if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }
        startTime();
    </script>

    <!-- DATATABLE -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.10.25/datatables.min.js"></script>

</html>