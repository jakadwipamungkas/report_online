<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Assessment Center Report Platform</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/web/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/web/css/components.css">

    <link rel="icon" href="<?= base_url() ?>assets/web/img/ptc-icon.png" type="image/x-icon">

    <?php
            if (isset($css) && count($css) > 0) {
                foreach ($css as $key => $vcss) {
                    echo '<link rel="stylesheet" href="' . (is_int($key) ? base_url($vcss) : $vcss) . '">';
                }
            }
        ?>

</head>