<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Pengembangan extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Pengembangan_model"
        ]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Pengembangan';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/pengembangan.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/pengembangan.css'
		);

		insertLog("Saran Pengembangan", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

	public function form_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Form Add Sarana Pengembangan';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/pengembangan.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/pengembangan.css'
		);

		insertLog("Form Saran Pengembangan", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'form');

	}

	public function form_edit_get()
	{
		// Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Form Edit Sarana Pengembangan';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/pengembangan.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/pengembangan.css'
		);

		insertLog("Form Edit Pengembangan", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'form_edit');
	}

	public function process_post()
	{
		$formData = $this->post();

        $data = [
            "competence_id"    => $formData["competence_name"],
            "behavior_id"      => $formData["behavior_name"],
            "mandiri"          => json_encode($formData["mandiri"]),
            "penugasan"        => json_encode($formData["penugasan"]),
            "training_seminar" => json_encode($formData["training"]),
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Pengembangan_model->save($data);
		redirect("pengembangan");
	}
}