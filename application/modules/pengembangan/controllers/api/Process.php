<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Pengembangan_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("pengembangan.*, competence.competence_name, behavior.behavior_name")
                        ->from("pengembangan")
                        ->join("competence", "competence.id = pengembangan.competence_id")
                        ->join("behavior", "behavior.id = pengembangan.behavior_id")
                        ->get()
                        ->result_array();
        
        foreach ($sql as $key => $value) {
            $sql[$key]["mandiri"]           = json_decode($value["mandiri"]);
            $sql[$key]["penugasan"]         = json_decode($value["penugasan"]);
            $sql[$key]["training_seminar"]  = json_decode($value["training_seminar"]);
        }
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function competence_get()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function behavior_get()
    {
        $sql = $this->db->select("id, behavior_name")
                        ->from("behavior")
                        ->where("competence_id", $this->get("competence_id"))
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function get_edit_get()
    {
        $sql = $this->db->select("pengembangan.*, competence.competence_name, behavior.behavior_name")
                        ->from("pengembangan")
                        ->join("competence", "competence.id = pengembangan.competence_id")
                        ->join("behavior", "behavior.id = pengembangan.behavior_id")
                        ->where("pengembangan.id", $this->get("pengembangan_id"))
                        ->get()
                        ->first_row();

        $sql->mandiri           = json_decode($sql->mandiri);
        $sql->penugasan         = json_decode($sql->penugasan);
        $sql->training_seminar  = json_decode($sql->training_seminar);

        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();

        $data = [
            "competence_id"    => $formData["competence_id"],
            "behavior_id"      => $formData["behavior_id"],
            "mandiri"          => $formData["mandiri"],
            "penugasan"        => $formData["penugasan"],
            "training_seminar" => $formData["training"],
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Pengembangan_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "competence_id"    => $formData["competence_id"],
            "behavior_id"      => $formData["behavior_id"],
            "mandiri"          => $formData["mandiri"],
            "penugasan"        => $formData["penugasan"],
            "training_seminar" => $formData["training"],
            "updated_by"       => $this->session->userdata("users")["fullname"],
            "updated_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Pengembangan_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Pengembangan_model->delete($formData["id"]);

        $this->response($sql);
    }
}
