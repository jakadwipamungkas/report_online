<div class="main-content" ng-controller="pengembangan">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Insert Saran Pengembangan</h4>
                  <div class="card-header-action">
                    
                  </div>
                </div>
                <form action="<?= base_url("pengembangan/process") ?>" method="POST">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="competence_name">Competence Name <span class="text-danger">*</span></label>
                                    <select class="form-control" name="competence_name" id="competence_name" ng-model="pengembangan.competence_id" ng-change="setBehavior()">
                                        <option value="" selected>-- Choose Competence --</option>
                                        <option ng-repeat="(kc, vc) in list_competence" ng-bind="vc.competence_name" value="{{vc.id}}"></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Behaviors <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-lg-11 col-md-11 col-sm-11">
                                            <select class="form-control" name="behavior_name" id="behavior_name" ng-model="pengembangan.behavior_id">
                                                <option value="" selected>-- Choose behavior --</option>
                                                <option ng-repeat="(kc, vc) in list_behavior" ng-bind="vc.behavior_name" value="{{vc.id}}"></option>
                                            </select>
                                        </div>
                                        <div class="col-1 col-md-1 col-lg-1 p-2" ng-show="loaddata">
                                            <i class="fas fa-spinner fa-spin text-info"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-repeat="(a, b) in pengembangan.mandiri track by $index">
                                    <label>Mandiri <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-10 col-md-10 col-lg-10">
                                            <input type="text" name="mandiri[]" id="mandiri" class="form-control choose_mandiri" ng-model="pengembangan.mandiri[a]">
                                        </div>
                                        <div class="col-2 col-md-2 col-lg-2">
                                            <!-- <a href="" ng-click="addRowMandiri()" class="btn btn-primary"><i class="fas fa-plus"></i></a> -->
                                            <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiMandiri('add',a)"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiMandiri('rm',a)"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-repeat="(a, b) in pengembangan.penugasan track by $index">
                                    <label>Penugasan <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-10 col-md-10 col-lg-10">
                                            <input type="text" name="penugasan[]" id="penugasan" class="form-control choose_penugasan" ng-model="pengembangan.penugasan[a]">
                                        </div>
                                        <div class="col-2 col-md-2 col-lg-2">
                                            <!-- <a href="" ng-click="addRowpenugasan()" class="btn btn-primary"><i class="fas fa-plus"></i></a> -->
                                            <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiPenugasan('add',a)"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiPenugasan('rm',a)"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-repeat="(a, b) in pengembangan.training_seminar track by $index">
                                    <label>Training / Seminar <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-10 col-md-10 col-lg-10">
                                            <input type="text" name="training[]" id="training" class="form-control choose_training" ng-model="pengembangan.training_seminar[a]">
                                        </div>
                                        <div class="col-2 col-md-2 col-lg-2">
                                            <!-- <a href="" ng-click="addRowtraining()" class="btn btn-primary"><i class="fas fa-plus"></i></a> -->
                                            <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiTraining('add',a)"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiTraining('rm',a)"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-header-action float-left">
                            <a href="<?= base_url("pengembangan") ?>" class="btn btn-danger"><i class="fas fa-times-circle"></i> Cancel</a>
                            <!-- <a href="#" class="btn btn-success btn-save" ng-click="save()"><i class="fas fa-check-circle"></i> Save</a> -->
                            <button type="submit" class="btn btn-success btn-save" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
    </section>
</div>