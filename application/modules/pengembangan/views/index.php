<div class="main-content" ng-controller="pengembangan">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Data Saran Pengembangan</h4>
                  <div class="card-header-action">
                    <input type="hidden" name="url" id="url" value="<?= $this->uri->segment(1) ?>">
                    <a href="<?= base_url() ?>pengembangan/form" class="btn btn-success"><i class="fas fa-plus"></i> Add</a>
                    <a href="#" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive" style="max-width:100% !important; overflow: auto;">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Kompetensi</th>
                                <th>Perilaku Utama</th>
                                <th>Mandiri</th>
                                <th>Penugasan</th>
                                <th>Training / Seminar</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="(key, val) in list_pengembangan">
                                <td ng-bind="(key+1)"></td>
                                <td ng-bind="val.competence_name"></td>
                                <td ng-bind="val.behavior_name"></td>
                                <td>
                                  <ul ng-repeat="(km, vm) in val.mandiri">
                                    <li ng-bind="vm"></li>
                                  </ul>
                                </td>
                                <td>
                                  <ul ng-repeat="(kp, vp) in val.penugasan">
                                    <li ng-bind="vp"></li>
                                  </ul>
                                </td>
                                <td>
                                  <ul ng-repeat="(kts, vts) in val.training_seminar">
                                    <li ng-bind="vts"></li>
                                  </ul>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit(key)" class="btn btn-sm btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" tabindex="-1" role="dialog" aria-labelledby="mdlDelete" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">
              <i class="fas fa-exclamation-circle text-warning mt-3 mb-5" style="font-size: 40px"></i><br>
              <h6>Are you sure want to delete this ?</h6><br>
              <table class="table table-striped">
                  <thead>
                      <tr>
                        <th>Kompetensi</th>
                        <th>Perilaku Utama</th>
                        <th>Mandiri</th>
                        <th>Penugasan</th>
                        <th>Training / Seminar</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                        <td ng-bind="deletepengembangan.competence_name"></td>
                        <td ng-bind="deletepengembangan.behavior_name"></td>
                        <td>
                          <ul ng-repeat="(km, vm) in deletepengembangan.mandiri">
                            <li ng-bind="vm"></li>
                          </ul>
                        </td>
                        <td>
                          <ul ng-repeat="(kp, vp) in deletepengembangan.penugasan">
                            <li ng-bind="vp"></li>
                          </ul>
                        </td>
                        <td>
                          <ul ng-repeat="(kts, vts) in deletepengembangan.training_seminar">
                            <li ng-bind="vts"></li>
                          </ul>
                        </td>
                      </tr>
                  </tbody>
              </table>
              <form>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="pengembangan_delete_id" ng-model="deletepengembangan.id">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="button" class="btn btn-primary btn-block m-0 btn-delete" ng-click="delete()"><i class="fas fa-check-circle"></i> Delete</button>
              </div>
            </div>
          </div>
      </div>
    </div> 
</div>