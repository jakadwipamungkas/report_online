<div class="main-content" ng-controller="pengembangan">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Update Saran Pengembangan</h4>
                  <input type="hidden" name="url" id="url" value="<?= $this->uri->segment(2) ?>">
                  <div class="card-header-action">
                    
                  </div>
                </div>
                <div class="card-body">
                    <form>
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="competence_name">Competence Name <span class="text-danger">*</span></label>
                                    <input type="hidden" name="idpengembangan" id="idpengembangan" ng-model="pengembangan.id">
                                    <select class="form-control" name="competence_name" id="competence_name" ng-model="pengembangan.competence_id" ng-change="setBehavior()">
                                        <option value="" selected>-- Choose Competence --</option>
                                        <option ng-repeat="(kc, vc) in list_competence" ng-bind="vc.competence_name" value="{{vc.id}}"></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Behaviors <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-lg-11 col-md-11 col-sm-11">
                                            <select class="form-control" name="behavior_name" id="behavior_name" ng-model="pengembangan.behavior_id">
                                                <option value="" selected>-- Choose behavior --</option>
                                                <option ng-repeat="(kc, vc) in list_behavior" ng-bind="vc.behavior_name" value="{{vc.id}}"></option>
                                            </select>
                                        </div>
                                        <div class="col-1 col-md-1 col-lg-1 p-2" ng-show="loaddata">
                                            <i class="fas fa-spinner fa-spin text-info"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-repeat="(a, b) in pengembangan.mandiri track by $index">
                                    <label>Mandiri <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-11 col-md-11 col-lg-11">
                                            <input type="text" name="mandiri" id="mandiri" class="form-control choose_mandiri" ng-model="pengembangan.mandiri[a]">
                                        </div>
                                        <div class="col-1 col-md-1 col-lg-1">
                                            <!-- <a href="" ng-click="addRowMandiri()" class="btn btn-primary"><i class="fas fa-plus"></i></a> -->
                                            <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiMandiri('add',a)"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiMandiri('rm',a)"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-repeat="(a, b) in pengembangan.penugasan track by $index">
                                    <label>Penugasan <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-11 col-md-11 col-lg-11">
                                            <input type="text" name="penugasan" id="penugasan" class="form-control choose_penugasan" ng-model="pengembangan.penugasan[a]">
                                        </div>
                                        <div class="col-1 col-md-1 col-lg-1">
                                            <!-- <a href="" ng-click="addRowpenugasan()" class="btn btn-primary"><i class="fas fa-plus"></i></a> -->
                                            <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiPenugasan('add',a)"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiPenugasan('rm',a)"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" ng-repeat="(a, b) in pengembangan.training_seminar track by $index">
                                    <label>Training / Seminar <span class="text-danger"> * </span></label>
                                    <div class="row">
                                        <div class="col-11 col-md-11 col-lg-11">
                                            <input type="text" name="training" id="training" class="form-control choose_training" ng-model="pengembangan.training_seminar[a]">
                                        </div>
                                        <div class="col-1 col-md-1 col-lg-1">
                                            <!-- <a href="" ng-click="addRowtraining()" class="btn btn-primary"><i class="fas fa-plus"></i></a> -->
                                            <button type="button" class="btn btn-success btn-add-crqnumber" ng-click="multiTraining('add',a)"><i class="fas fa-plus"></i></button>
                                            <button type="button" class="btn btn-danger btn-del-crqnumber"  ng-click="multiTraining('rm',a)"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-header-action float-left">
                        <a href="<?= base_url("pengembangan") ?>" class="btn btn-danger"><i class="fas fa-times-circle"></i> Cancel</a>
                        <a href="#" class="btn btn-success btn-update" ng-click="update()"><i class="fas fa-check-circle"></i> Update</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
</div>