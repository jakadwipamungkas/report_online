<div class="main-content" ng-controller="dashboard">
    <section class="section">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Users</h4>
                        </div>
                        <div class="card-body link-dashboard">
                            <a href="<?= base_url("users") ?>"><span ng-bind="datausers.total_users"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="far fa-newspaper"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Assessment</h4>
                        </div>
                        <div class="card-body">
                            <span ng-bind="totalParticipant"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-file"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Reports</h4>
                        </div>
                        <div class="card-body">
                            <span ng-bind="datausers.total_report"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-circle"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Online Users</h4>
                        </div>
                        <div class="card-body link-dashboard">
                            <a href="" ng-click="showOnline()"><span ng-bind="datausers.total_online"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Monitoring Assessment</h4>
                  <div class="card-header-action">
                    <div class="btn-group">
                      <a href="#" class="btn btn-success">Export</a>
                      <a href="#" class="btn btn-primary">Filter</a>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Nomor Peserta</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Assessor</th>
                                <th>Quality Control</th>
                                <!-- <th>Publish Date</th> -->
                                <?php //if ($users["role_id"] == 3) { ?>
                                    <th>Action</th>
                                <?php //} ?>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="(key, val) in dataParticipant">
                                <td ng-bind="(key+1)"></td>
                                <td ng-bind="val.participant_number"></td>
                                <td ng-bind="val.participant_name"></td>
                                <td ng-bind="val.position"></td>
                                <td ng-if="val.assessor_id == NULL">
                                    <?php if ($users["role_id"] == 1) { ?>
                                        <a href="<?= base_url("assessor") ?>"><span class="btn btn-sm btn-warning"> set assessor </span></a>
                                    <?php } ?>
                                </td>
                                <td ng-if="val.assessor_id != NULL">
                                    <span ng-bind="val.assessor_name"></span>
                                </td>
                                <td ng-if="val.qc_id == NULL">
                                    <?php if ($users["role_id"] == 1) { ?>
                                        <a href="<?= base_url("qualitycontrol") ?>"><span class="btn btn-sm btn-warning"> set quality control </span></a>
                                    <?php } ?>
                                </td>
                                <td ng-if="val.qc_id != NULL">
                                    <span ng-bind="val.qc_name"></span>
                                </td>
                                <!-- <td ng-bind="val.created_at"></td> -->
                                <?php //if ($users["role_id"] == 3) { ?>
                                    <td ng-if="val.status_report == 1">
                                        <div class="btn-group">
                                            <a href="<?= base_url() ?>resources/report/{{val.report_name}}" class="btn btn-sm btn-info"><i class="fas fa-book"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                        </div>
                                    </td>
                                    <td ng-if="val.status_report == 0">
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-sm btn-info disabled"><i class="fas fa-book"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                                        </div>
                                    </td>
                                <?php //} ?>
                              </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
    <div class="modal fade" id="mdlOnline" tabindex="-1" role="dialog" aria-labelledby="mdlOnline" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">
              <h6>List Users Online</h6><br>
              <table class="table table-striped">
                  <thead>
                      <tr>
                        <th>No</th>
                        <th>Fullname</th>
                        <th>Login Date</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr ng-repeat="(kol, vol) in datausers.data_online">
                        <td ng-bind="(kol+1)"></td>
                        <td ng-bind="vol.fullname"></td>
                        <td ng-bind="vol.last_login"></td>
                  </tbody>
              </table>
              <form>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="pengembangan_delete_id" ng-model="deletepengembangan.id">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-secondary btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                <!-- <button type="button" class="btn btn-primary btn-block m-0 btn-delete" ng-click="delete()"><i class="fas fa-check-circle"></i> Delete</button> -->
              </div>
            </div>
          </div>
      </div>
    </div> 
</div>