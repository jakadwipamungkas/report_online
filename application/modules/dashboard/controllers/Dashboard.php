<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dashboard extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Monitoring Assessment';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/monitoring.js?' . rand()
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/monitoring.css?' . rand()
		);

		insertLog("Monitoring Assessment", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}
}
