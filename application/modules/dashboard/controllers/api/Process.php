<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            // "Users_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("*")
                                ->from("users")
                                ->get()
                                ->result_array();
        
        foreach ($sql as $key => $value) {
            if ($value["status_login"] == "online") {
                $data_online[] = $value;
            }
        }

        $cnt_report = $this->db->select("COUNT(*) as total")->from("master_reports")->get()->result_array();
        
        $return = [
            "total_users"   => count($sql),
            "total_online"  => count($data_online),
            "data_online"   => $data_online,
            "total_report"  => $cnt_report[0]["total"]
        ];

        $this->response($return);

	}

    public function participant_get()
    {
        if ($this->session->userdata("users")["role_id"] == 1) {

            $sql = $this->db->select("participant.*, ua.fullname as assessor_name, uq.fullname as qc_name")
                                    ->from("participant")
                                    ->join("users ua", "ua.id = participant.assessor_id", "LEFT")
                                    ->join("users uq", "uq.id = participant.qc_id", "LEFT")
                                    ->get()
                                    ->result_array();

        } else {

            $sql = $this->db->select("participant.*, ua.fullname as assessor_name, uq.fullname as qc_name, mr.report_name")
                                    ->from("participant")
                                    ->join("users ua", "ua.id = participant.assessor_id", "LEFT")
                                    ->join("users uq", "uq.id = participant.qc_id", "LEFT")
                                    ->join("master_reports mr", "mr.participant_id = participant.id", "LEFT")
                                    // ->where("assessor_id", $this->session->userdata("users")["id"])
                                    ->get()
                                    ->result_array();

        }
        
        $return = [
            "data"  => $sql,
            "total" => count($sql)
        ];

        $this->response($return);
    }
}
