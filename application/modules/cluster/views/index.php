<div class="main-content" ng-controller="cluster">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Data Cluster</h4>
                  <div class="card-header-action">
                    <a href="" class="btn btn-success" ng-click="clickAdd()"><i class="fas fa-plus"></i> Add</a>
                    <a href="" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Cluster Name</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <!-- <tr ng-repeat="(key, val) in list_cluster">
                                <td class="text-center" ng-bind="(key+1)"></td>
                                <td ng-bind="val.cluster_name"></td>
                                <td class="text-center">
                                  <div class="btn-group">
                                        <a href="" ng-click="clickEdit(val)" class="btn btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.cluster_name)" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr> -->
                              <?php foreach ($list_cluster["data"] as $key => $value) { ?>
                                <tr>
                                  <td class="text-center"><?= ($key+1) ?></td>
                                  <td class="text-center"><?= $value["cluster_name"] ?></td>
                                  <td class="text-center">
                                    <div class="btn-group">
                                          <a href="" ng-click="clickEdit('<?= base64_encode(json_encode($value)) ?>')" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i> Update</a>
                                          <a href="" ng-click="clickDelete('<?= $value["id"] ?>','<?= $value["cluster_name"] ?>')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                      </div>
                                  </td>
                                </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>

    <!-- Modal Add -->
    <div class="modal fade" id="mdlAdd" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAddLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAddLabel">Add Cluster</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="<?= base_url("cluster/process") ?>" method="POST">
            <div class="modal-body">
                <div class="form-group">
                  <label for="cluster_name">Cluster Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="cluster_name" name="cluster_name" required>
                </div>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="submit" class="btn btn-primary btn-block m-0 btn-save" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="mdlEdit" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlEditLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlEditLabel">Add Cluster</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="cluster_name_edit">Cluster Name <span class="text-danger">*</span></label>
                  <input type="hidden" class="form-control" id="cluster_name_id" ng-model="editcluster.id">
                  <input type="text" class="form-control" id="cluster_name_edit" ng-model="editcluster.cluster_name">
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0 btn-update" ng-click="update()"><i class="fas fa-check-circle"></i> Update</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="mdlDelete" tabindex="-1" role="dialog" aria-labelledby="mdlDelete" aria-hidden="true">
      <div class="modal-dialog modal-sm modal-dialog-centered">
          <div class="modal-content mt-5" style="background: #ffffff !important; color: #000 !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">
              <i class="fas fa-exclamation-circle text-warning mt-3 mb-5" style="font-size: 40px"></i><br>
              <h6>Are you sure want to delete this Cluster <b class="text-danger" ng-bind="deletecluster.cluster_name"></b> ?</h6>
              <form>
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="cluster_name_delete_id" ng-model="deletecluster.id">
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="button" class="btn btn-primary btn-block m-0 btn-delete" ng-click="delete()"><i class="fas fa-check-circle"></i> Delete</button>
              </div>
            </div>
          </div>
      </div>
    </div> 
</div>