<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Cluster_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("*")
                        ->from("clusters")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function index_post()
    {
        $formData = $this->post();

        $data = [
            "cluster_name"  => $formData["name"],
            "created_by"    => $this->session->userdata("users")["fullname"],
            "created_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Cluster_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "cluster_name"  => $formData["cluster_name"],
            "updated_by"    => $this->session->userdata("users")["fullname"],
            "updated_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Cluster_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Cluster_model->delete($formData["id"]);

        $this->response($sql);
    }
}
