<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cluster extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Cluster_model"
        ]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Cluster';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/cluster.js?'. rand()
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/cluster.css?'. rand()
		);

		$this->data['list_cluster'] = $this->Cluster_model->get();

		insertLog("Cluster", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

	public function process_post()
	{
		$formData = $this->post();

        $data = [
            "cluster_name"  => $formData["cluster_name"],
            "created_by"    => $this->session->userdata("users")["fullname"],
            "created_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Cluster_model->save($data);
		redirect("cluster");
	}
}
