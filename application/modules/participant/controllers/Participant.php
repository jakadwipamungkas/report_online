<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Participant extends Iacr_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Participant_model"
        ]);
	}

	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'participant';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			'assets/app/js/part.js'
		);

		$this->data['css'] = array(
			'assets/app/css/participant.css'
		);

		$getUser = $this->Participant_model->get()["data"];
		
		$this->data['list_user']	= $getUser;
		
		insertLog("Participant List", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

}
