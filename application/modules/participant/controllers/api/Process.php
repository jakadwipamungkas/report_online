<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Participant_model"
        ]);
	}

    public function import_file_post()
    {
        // print_r("<pre>");
        // print_r($_FILES);
        // exit();
        if (isset($_FILES["file_import"]["name"])) {
			$path = $_FILES["file_import"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				
				for($row=2; $row<=$highestRow; $row++)
				{
					$participant_name   = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$participant_number = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$position           = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$temp_data[] = array(
                        "participant_name"			=> $participant_name,
                        "participant_number"		=> $participant_number,
                        "position"					=> $position,
                        "participant_gelar_jurfak"	=> NULL,
                        "status"					=> 1,
                        "status_report"				=> 0,
					); 	
				}
			}

			$insert = $this->Participant_model->save_import($temp_data);

			if($insert){

				$path = 'resources/participant/';
			
				$temp = explode(".", $_FILES["file_import"]["name"]);
				$filename = date("Ymd_His")."_".$this->session->userdata("username") ."_". $_FILES["file_import"]["name"];

				if (!file_exists($path) && !is_dir($path)) {
					mkdir($path, 0777, true);
				}

				move_uploaded_file($_FILES['file_import']['tmp_name'], $path . '/' . $filename);

				redirect("participant");
			}else{
				redirect("participant");
			}
		} else{
			redirect("participant");
		}
    }
}
