<div class="main-content" ng-controller="participant_list">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Participant List</h4>
                  <div class="card-header-action">
                    <!-- <a href="" class="btn btn-success"><i class="fas fa-plus"></i> Add</a> -->
                    <a href="" class="btn btn-outline-info" style="border-radius: 5px !important;"><i class="fas fa-download"></i> Export</a>
                    <a href="" class="btn btn-outline-danger" data-toggle="modal" data-target="#mdlImport" style="border-radius: 5px !important;"><i class="fas fa-file-excel"></i> Import</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="tbl-participant">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Participant Name</th>
                                <th>Participant Number</th>
                                <th>Last Education</th>
                                <th>Position</th>
                                <th>Assessor Name</th>
                                <th>Quality Control Name</th>
                                <!-- <th>Action</th> -->
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($list_user as $key => $value) { ?>
                                <tr>
                                  <td><?= ($key+1) ?></td>
                                  <td class="text-left"><?= $value["participant_name"] ?></td>
                                  <td><?= $value["participant_number"] ?></td>
                                  <td><?= $value["participant_gelar_jurfak"] ?></td>
                                  <td><?= $value["position"] ?></td>
                                  <td><?= $value["assessor_name"] ?></td>
                                  <td><?= $value["qc_name"] ?></td>
                                  <!-- <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit('<?= base64_encode(json_encode($value)) ?>')" class="btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
                                        <a href="" ng-click="clickDelete(val.id, val.fullname)" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                                    </div>
                                  </td> -->
                                </tr>
                              <?php } ?>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>

    <!-- Modal Add -->
    <div class="modal fade" id="mdlImport" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlImportLabel" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlImportLabel">Import Participant</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="<?= base_url() ?>import_file" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label for="file_import">Choose File <span class="text-danger">*</span></label>
                <input type="file" class="form-control" id="file_import" name="file_import" required>
              </div>
            </div>
            <div class="modal-footer">
              <div class="btn-group col-12" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
                <button type="submit" class="btn btn-primary btn-block m-0 btn-save"><i class="fas fa-check-circle"></i> Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>