<?php
class Participant_model extends CI_Model {

    public function get()
    {
        if ($this->session->userdata("users")["role_id"] == 1) {

            $sql = $this->db->select("participant.*, ua.fullname as assessor_name, uq.fullname as qc_name")
                                    ->from("participant")
                                    ->join("users ua", "ua.id = participant.assessor_id", "LEFT")
                                    ->join("users uq", "uq.id = participant.qc_id", "LEFT")
                                    ->get()
                                    ->result_array();

        } else {

            $sql = $this->db->select("participant.*, ua.fullname as assessor_name, uq.fullname as qc_name, mr.report_name")
                                    ->from("participant")
                                    ->join("users ua", "ua.id = participant.assessor_id", "LEFT")
                                    ->join("users uq", "uq.id = participant.qc_id", "LEFT")
                                    ->join("master_reports mr", "mr.participant_id = participant.id", "INNER")
                                    ->where("assessor_id", $this->session->userdata("users")["id"])
                                    ->get()
                                    ->result_array();

        }
        
        $return = [
            "data"  => $sql,
            "total" => count($sql)
        ];

        return $return;
    }

    public function save_import($data)
    {
        foreach ($data as $key => $value) {
            $dt = [
                "participant_name"			=> $value["participant_name"],
                "participant_number"		=> $value["participant_number"],
                "position"					=> $value["position"],
                "participant_gelar_jurfak"	=> NULL,
                "status"					=> 1,
                "status_report"				=> 0,
                "created_at"                => date("Y-m-d H:i:s"),
                "created_by"                => $this->session->userdata("username")
            ];

            $sql = $this->db->insert("participant", $dt);
        }

        if ($sql) {
            return $return = [
                "status"    => "OK",
                "msg"       => "Surat keluar berhasil dibuat."
            ];
        } else {
            return $return = [
                "status"    => "Failed",
                "msg"       => "Gagal dibuat."
            ];
        }
    }
}