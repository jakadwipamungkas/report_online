<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Behavior extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model([
            "Behavior_model"
        ]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Behavior';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/behavior.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/behavior.css'
		);

		$this->data["list_behavior"] = $this->Behavior_model->get();
		$this->data["list_competence"] = $this->Behavior_model->getCompetence();

		insertLog("Behavior", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}

	public function process_post()
	{
		$formData = $this->post();

        $data = [
            "behavior_name"    => $formData["behavior_name"],
            "behavior_desc"    => $formData["behavior_desc"],
            "competence_id"    => $formData["competence_id"],
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Behavior_model->save($data);
		redirect("behavior");
	}
}
