<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Behavior_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("behavior.*, competence.competence_name")
                        ->from("behavior")
                        ->join("competence", "competence.id = behavior.competence_id")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function competence_get()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();

        $data = [
            "behavior_name"    => $formData["behavior_name"],
            "behavior_desc"    => $formData["behavior_desc"],
            "competence_id"    => $formData["competence_id"],
            "created_by"       => $this->session->userdata("users")["fullname"],
            "created_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Behavior_model->save($data);

        $this->response($sql);
    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "behavior_name"    => $formData["behavior_name"],
            "behavior_desc"    => $formData["behavior_desc"],
            "competence_id"    => $formData["competence_id"],
            "updated_by"       => $this->session->userdata("users")["fullname"],
            "updated_at"       => date("Y-m-d H:i:s")
        ];

        $sql = $this->Behavior_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Behavior_model->delete($formData["id"]);

        $this->response($sql);
    }
}
