<?php
class Behavior_model extends CI_Model {

    public function get()
    {
        $sql = $this->db->select("behavior.*, competence.competence_name")
                        ->from("behavior")
                        ->join("competence", "competence.id = behavior.competence_id")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function getCompetence()
    {
        $sql = $this->db->select("id, competence_name")
                        ->from("competence")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        return $return;
    }

    public function save($data)
    {

        $query = $this->db->insert("behavior", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully created, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function update($id, $data)
    {

        $query = $this->db->where("id", $id)->update("behavior", $data);

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully updated, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

    public function delete($id)
    {

        $query = $this->db->where("id", $id)->delete("behavior");

        if ($query) {
            $return = [
                "error"     => false,
                "message"   => "Data successfully deleted, please wait !"
            ];
        } else {
            $return = [
                "error"     => true,
                "message"   => "Failed, Please Try Again"
            ];
        }

        return $return;
    }

}