<div class="form-body">
    <div class="row">
        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5 bg-primary">
            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                <img class="img-fluid" style="max-height: 600px" src="https://ontest.pertamina-ptc.com/app-assets/images/ptc/login-page-v2.png" alt="Intro" />
            </div>
        </div>
        <div class="d-lg-flex mt-5 col-lg-4 p-5">
            <div class="col-12">
                <div class="text-center">
                    <img src="<?= base_url('assets/login/images/logo-ptc.png') ?>" class="img-fluid" alt="Responsive image">
                    <h3 style="text-align: center !important;" class="mt-2 mb-3">Report Assessment Center Online</h3>
                </div>
                <?= $this->session->flashdata('message'); ?>
                <div class="login-form">
                    <form class="user" action="<?= base_url('login'); ?>" method="POST">
                        <div class="form-group form-floating-label">
                            <input id="username" name="username" type="text" class="form-control input-border-bottom" required>
                            <label for="username" class="placeholder">Username</label>
                        </div>
                        <div class="form-group form-floating-label">
                            <input id="password" name="password" type="password" class="form-control input-border-bottom" required>
                            <label for="password" class="placeholder">Password</label>
                            <div class="show-password">
                                <i class="flaticon-interface"></i>
                            </div>
                        </div>
                        <div class="row form-sub m-0">
                            <a href="#" class="link float-right"></a>
                        </div>
                        <div class="form-action mb-3 mt-3 text-center">
                            <button type="submit" class="btn btn-primary btn-block btn-login">Sign In</button>
                        </div>
                        <div class="login-account text-center">
                            <span class="msg" style="font-size: 13px;">Don't have an account yet ? <br> Contact Administrator.</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>