<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

class Login extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index_get()
	{

	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Login';
		$this->data['page'] = 'login';

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/login.css'
		);

		$this->template->load($this->data, "login", 'index');

	}

	public function index_post()
	{
		// print_r($_POST["password"]);
		// print_r("<br>");
		// print_r(setEncrypt($_POST["password"]));
		// exit();
		$this->load->model('UsersDB');

		$error = true;

		if($_POST['username'] == null and $_POST['password'] == null){

			$this->response(array('error' => true, 'message' => 'Please fill username and password!'));

		}else{
			
			$users = $this->UsersDB->getUser($_POST['username']);

			if ($users) {

		    	$check = setDecrypt($users->password, $_POST['password']);

		    	if ($check) {

					$data = array(
						'last_login' => date("Y-m-d H:i:s")
					);

					$this->db->where('username', $_POST['username']);
					$this->db->update('users', $data);

		    		$this->session->set_userdata([
							'users' => [
								'id'		=> $users->id,
								'fullname'	=> $users->fullname,
								'username'	=> $users->username,
								'email'	    => $users->email,
								'role_id'	=> $users->role_id
							]
					]);

		    		$error = false;
		    		$message = 'User valid, please wait!';

					$this->db->where("id", $users->id)->update("users", [
																			"last_login"	=> date("Y-m-d H:i:s"),
																			"status_login"	=> "online"
																		]);

					if ($users->role_id == 1) {
						redirect(base_url('dashboard'));
					} elseif ($users->role_id == 2) {
						redirect(base_url('assessment'));
					} elseif ($users->role_id == 3) {
						redirect(base_url('dashboard'));
					} elseif ($users->role_id == 4) {
						redirect(base_url('dashboard'));
					}

		    	}else{
		    		$error = true;
		    		$message = 'Username or password wrong!';
					redirect(base_url());
		    	}

			}else{
				$error = true;
				$message = 'User not found!';
				redirect(base_url());
			}

			// $this->set_response(['error' => $error, 'message' => $message, 'url' => $url]);
		}
	}

	public function logout_get()
	{
		if($this->db->where("id", $this->session->userdata('users')["id"])->update("users", ["status_login"	=> "offline"])){
			
			$this->session->sess_destroy();
			redirect(base_url());

		}

	}
}
