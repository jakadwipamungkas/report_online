<div class="main-content" ng-controller="assessor">
    <section class="section">
        <!-- Data Assessor  -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Data Assessor</h4>
                  <div class="card-header-action">
                    <a href="" class="btn btn-success" ng-click="clickAdd()"><i class="fas fa-plus"></i> Add</a>
                    <a href="" class="btn btn-info"><i class="fas fa-download"></i> Export</a>
                  </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                          <thead>
                              <tr>
                                <th>No</th>
                                <th>Participant Name</th>
                                <th>Assessor Name</th>
                                <th>Assessor Degree</th>
                                <th>Assessor SIPP</th>
                                <th>Status Assessor</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="(key, val) in list_assessor">
                                <td ng-bind="(key+1)"></td>
                                <td class="text-left" ng-bind="val.participant_name"></td>
                                <td ng-bind="val.fullname"></td>
                                <td ng-bind="val.assessor_degree"></td>
                                <td ng-bind="val.assessor_sipp"></td>
                                <td ng-if="val.status == 'active'">
                                  <span class="badge badge-success">Active</span>
                                </td>
                                <td ng-if="val.status == 'nonactive'">
                                  <span class="badge badge-danger">Non active</span>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" ng-click="clickEdit(val)" class="btn btn-warning"><i class="fas fa-pen"></i></a>
                                        <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                    </div>
                                </td>
                              </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
        </div>

        <!-- Tracking Assessor -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>Tracking Assessor</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" datatable="ng">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Participant Name</th>
                                <th>Assessor Name</th>
                                <th>Status Report</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody ng-repeat="(kl, vl) in list_assessor">
                              <tr>
                                <td ng-bind="(kl+1)"></td>
                                <td class="text-left" ng-bind="vl.participant_name"></td>
                                <td ng-bind="vl.fullname"></td>
                                <td ng-if="vl.status_report == 'waiting' || vl.status_report == 'On Going'">
                                  <span class="badge badge-warning text-uppercase" ng-bind="vl.status_report"></span>
                                </td>
                                <td ng-if="vl.status_report == 'finished' || vl.status_report == 'On Going'">
                                  <span class="badge badge-success text-uppercase" ng-bind="vl.status_report"></span>
                                </td>
                                <td>
                                    <div class="btn-group" ng-if="vl.status_report == 'waiting' || vl.status_report == 'On Going'">
                                        <!-- <a href="" class="btn btn-info disabled"><i class="fas fa-eye"></i></a> -->
                                        <a href="" class="btn btn-success disabled"><i class="fas fa-print"></i></a>
                                    </div>
                                    <div class="btn-group" ng-if="vl.status_report == 'finished'">
                                        <!-- <a href="" class="btn btn-info"><i class="fas fa-eye"></i></a> -->
                                        <a href="<?= base_url() ?>resources/report/{{vl.report_name}}" class="btn btn-success"><i class="fas fa-print"></i></a>
                                    </div>
                                </td>
                              </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="mdlAssessor" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlAssessorLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlAssessorLabel">Add Assessor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="participant_name">Participant Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="assessor_name" ng-model="add.participant_name">
                    <option value="" selected>-- Choose Participant --</option>
                    <option ng-repeat="(kp, vp) in dataParticipant" ng-bind="vp.participant_name" value="{{vp.id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="assessor_name">Assessor Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="assessor_name" ng-model="add.user_id" ng-change="others()">
                    <option value="" selected>Assessor Name</option>
                    <option ng-repeat="(kus, vus) in list_users" ng-bind="vus.fullname" value="{{vus.id}}"></option>
                    <option value="others">Add Assessor</option>
                  </select>
                </div>
                <div class="form-group frm_others d-none">
                  <label for="usr_assessorname">Assessor Name <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="usr_assessorname" ng-model="add.usr_assessorname">
                </div>
                <div class="form-group frm_others d-none">
                  <label for="username">Username <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="username" ng-model="add.username">
                </div>
                <div class="form-group frm_others d-none">
                  <label for="email">Email <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="email" ng-model="add.email">
                </div>
                <div class="form-group frm_others d-none">
                  <label for="password">Password <span class="text-danger">*</span></label>
                  <input type="password" class="form-control" id="password" ng-model="add.password" readonly>
                </div>
                <div class="form-group">
                  <label for="assessor_degree">Assessor Degree <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="assessor_degree" ng-model="add.degree">
                </div>
                <div class="form-group">
                  <label for="assessor_sipp">Assessor SIPP <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="assessor_sipp" ng-model="add.sipp">
                </div>
                <div class="form-group">
                  <label for="assessor_sipp">Status Assessor<span class="text-danger">*</span></label>
                  <select class="custom-select" ng-model="add.status">
                    <option selected>-- Choose Status --</option>
                    <option value="active">Active</option>
                    <option value="non-active">Non Active</option>
                  </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="mdlEdit" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlEditLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mdlEditLabel">Edit Assessor</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="participant_name">Participant Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="assessor_name" ng-model="edit.participant_id">
                    <option value="" selected>-- Choose Participant --</option>
                    <option ng-repeat="(kp, vp) in dataParticipant" ng-bind="vp.participant_name" value="{{vp.id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="assessor_name">Assessor Name <span class="text-danger">*</span></label>
                  <select class="form-control" id="assessor_name" ng-model="edit.user_id">
                    <option value="" selected>Assessor Name</option>
                    <option ng-repeat="(kus, vus) in list_users" ng-bind="vus.fullname" value="{{vus.id}}"></option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="assessor_degree">Assessor Degree <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="assessor_degree" ng-model="edit.assessor_degree">
                </div>
                <div class="form-group">
                  <label for="assessor_sipp">Assessor SIPP <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="assessor_sipp" ng-model="edit.assessor_sipp">
                </div>
                <div class="form-group">
                  <label for="assessor_sipp">Status Assessor<span class="text-danger">*</span></label>
                  <select class="custom-select" ng-model="edit.status">
                    <option selected>-- Choose Status --</option>
                    <option value="active">Active</option>
                    <option value="non-active">Non Active</option>
                  </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="btn-group col-12" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-danger btn-block m-0" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancel</button>
              <button type="button" class="btn btn-primary btn-block m-0" ng-click="save()"><i class="fas fa-check-circle"></i> Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- END -->
</div>