<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Assessor extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Assessor';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/app/js/assessor.js?' . rand()
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
			'assets/app/css/assessor.css?' . rand()
		);

		insertLog("Assessor", base_url() . $this->uri->segment(1));

		$this->template->load($this->data, null, 'index');

	}
}
