<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Process extends Iacr_Controller {

	
	function __construct()
	{
		parent::__construct();
        $this->load->model([
            "Assessor_model"
        ]);
	}

	
	public function index_get()
	{
        $sql = $this->db->select("assessor.*, users.fullname, users.status, participant.participant_name, master_reports.report_name")
                        ->from("assessor")
                        ->join("users", "users.id = assessor.user_id")
                        ->join("participant", "participant.id = assessor.participant_id")
                        ->join("master_reports", "master_reports.participant_id = assessor.participant_id", "left")
                        ->get()
                        ->result_array();

        // print_r("<pre>");
        // print_r($sql);
        // exit();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}
 
    public function participant_get()
	{
        $sql = $this->db->select("*")
                        ->from("participant")
                        ->where("assessor_id IS NULL")
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);

	}

    public function users_data_get()
    {
        $sql = $this->db->select("id, fullname, username")
                        ->from("users")
                        ->where("role_id", 3)
                        ->get()
                        ->result_array();
        
        $return = [
            "data" => $sql
        ];

        $this->response($return);
    }

    public function index_post()
    {
        $formData = $this->post();
        // print_r("<pre>");
        // print_r($formData);
        // exit();

        if ($formData["user_id"] == "others") {
            $dt_user = [
                "fullname"  => $formData["usr_assessorname"],
                "username"  => $formData["username"],
                "email"     => $formData["email"],
                "role_id"   => 3,
                "password"  => setEncrypt($formData["password"]),
                "temp_pass" => $formData["password"],
                "status"    => "active",
                "created_by"    => $this->session->userdata("users")["fullname"],
                "created_at"    => date("Y-m-d H:i:s")
            ];

            $crt_usr = $this->db->insert("users", $dt_user);
            $insert_id = $this->db->insert_id();

            $data = [
                "user_id"           => $insert_id,
                "status"            => $formData["status"],
                "participant_id"    => $formData["participant_name"],
                "assessor_degree"   => $formData["degree"],
                "assessor_sipp"     => $formData["sipp"],
                "status_report"     => "waiting",
                "created_by"        => $this->session->userdata("users")["fullname"],
                "created_at"        => date("Y-m-d H:i:s")
            ];

            $sql = $this->Assessor_model->save($data);

            $this->response($sql);

        } else {

            $data = [
                "user_id"           => $formData["user_id"],
                "status"            => $formData["status"],
                "participant_id"    => $formData["participant_name"],
                "assessor_degree"   => $formData["degree"],
                "assessor_sipp"     => $formData["sipp"],
                "status_report"     => "waiting",
                "created_by"        => $this->session->userdata("users")["fullname"],
                "created_at"        => date("Y-m-d H:i:s")
            ];
    
            $sql = $this->Assessor_model->save($data);

            $this->response($sql);
        }

    }

    public function update_post()
    {
        $formData = $this->post();

        $data = [
            "user_id"           => $formData["user_id"],
            "status"            => $formData["status"],
            "participant_name"  => $formData["participant_name"],
            "assessor_degree"   => $formData["degree"],
            "assessor_sipp"     => $formData["sipp"],
            "status_report"     => "waiting",
            "updated_by"    => $this->session->userdata("users")["fullname"],
            "updated_at"    => date("Y-m-d H:i:s")
        ];

        $sql = $this->Assessor_model->update($formData["id"], $data);

        $this->response($sql);
    }

    public function delete_post()
    {
        $formData = $this->post();
        
        $sql = $this->Assessor_model->delete($formData["id"]);

        $this->response($sql);
    }
}
