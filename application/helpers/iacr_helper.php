<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * url in parameter.
 * @return void
 */
if ( ! function_exists('myUrlEncode'))
{   
    function myUrlEncode($string) {
        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D', '%E2%80%99');
        $replacements = "-";
        return str_replace('+', '-', str_replace($entities, $replacements, urlencode(strtolower($string))));
    }
}

if( ! function_exists('setEncrypt'))
{
    function setEncrypt($psswd) {
        
        // A higher "cost" is more secure but consumes more processing power
        $cost = 8;

        // Create a random salt
        $salt = strtr(base64_encode(bin2hex(random_bytes($cost))), '+', '.');

        // Prefix information about the hash so PHP knows how to verify it later.
        // "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
        $salt = sprintf("$2a$%02d$", $cost) . $salt;

        // Hash the password with the salt
        $hash = crypt($psswd, $salt);

        return $hash;

    }

}

if( ! function_exists('convmont'))
{
    function convmont($month) {
        $conv_month = "";
        
        if ($month == "January") {
            $conv_month = "Januari";
        } 
        elseif ($month == "February") {
            $conv_month = "Februari";
        }
        elseif ($month == "March") {
            $conv_month = "Maret";
        }
        elseif ($month == "April") {
            $conv_month = "April";
        }
        elseif ($month == "May") {
            $conv_month = "Mei";
        }
        elseif ($month == "June") {
            $conv_month = "Juni";
        }
        elseif ($month == "July") {
            $conv_month = "Juli";
        }
        elseif ($month == "August") {
            $conv_month = "Agustus";
        }
        elseif ($month == "September") {
            $conv_month = "September";
        }
        elseif ($month == "October") {
            $conv_month = "Oktober";
        }
        elseif ($month == "November") {
            $conv_month = "November";
        }
        elseif ($month == "December") {
            $conv_month = "Desember";
        }

        return $conv_month;

    }

}

if( ! function_exists('insertLog'))
{
    function insertLog($page, $vmain) {  
        $ci=& get_instance();
        $ci->load->database(); 
    
        $ip = get_client_ip();

        $fullname = $ci->session->userdata('users')['fullname'];             
        
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $access_time = date('Y-m-d H:i:s');
    
        $logdata = array(
            'ip' => $ip,
            'fullname' => $fullname,
            'browser' => $browser,
            'modul' => $page,
            'vmain' => $vmain,
            'access_time' => $access_time,
        );
    
        $sql = $ci->db->insert('users_log', $logdata);
        
        return $sql;
    }
}

if( ! function_exists('setDecrypt'))
{
    function setDecrypt($hash, $psswd) {

        if (hash_equals($hash, crypt($psswd, $hash))) {
           return true;
        }else
            return false;

    }
}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

?>